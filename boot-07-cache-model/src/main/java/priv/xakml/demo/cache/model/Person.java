package priv.xakml.demo.cache.model;

import lombok.Data;

/**
 * @author lhx
 * @date 2021-08-04 14:14
 **/
@Data
public class Person {

    /**
     * id
     */
    private long id;

    /**
     * 名称
     */
    private String name;


    /**
     * 初始化Person类型
     */
    public Person() {
    }

    /**
     * 初始化Person类型
     * @param id id
     * @param name 名称
     */
    public Person(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
