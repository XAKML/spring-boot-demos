## spring boot mysql模块示例项目
> 2021-11-17 19:00:02  
>       1. 添加事务支持  
> 2021-11-18 22:01:39  
>       在mybatis的insert标签中，默认情况下返回的值为sql脚本影响到的行数，如果遇到values子句多条记录的情况下，依旧返回sql语句影响到的行数。  
> 
>       尚有一点疑问： 在mysql中，更新（update）语句，如果遇到需要更新的字段原始值与最新值一致的情况下，是否返回影响行数为零的结果？ 有待验证


#### 使用mybatis批量保存数据
使用如下示例的sql脚本
```xml
    <!--使用时请注意： foreach元素中的close和open属性不需要添加起始括号  open="(" close=")" -->
    <insert id="insertBatch" parameterType="java.util.List">
        insert into Table1 (CaiNiaoZyTraceId, ReceptacleId,MailClass,ReceptacleWeight)
        VALUES
        <foreach collection="list" item="receive_item" separator=",">
            （#{receive_item.caiNiaoZyTraceId},#{receive_item.receptacleId},#{receive_item.mailClass},#{receive_item.receptacleWeight}）
        </foreach>
    </insert>
```

使用如下脚本自动填充自增主键对应的实体类成员字段值
```xml
<!--注意insert标签的以下属性： useGeneratedKeys="true" keyProperty="id" -->
 <insert id="insertBatch" parameterType="java.util.List" useGeneratedKeys="true" keyProperty="id">
        insert into test(createtime) values
        <foreach collection="list" item="item" separator=",">
            (#{item.createtime})
        </foreach>
</insert>
```




**使用上述sql脚本，在Java代码中对应的方法返回值为受到影响的行数，可以使用long和int来接收**
```java
public interface ITable1{
    /**
     *  批量保存数据对象
     * @param list 需要保存的对象集合
     * @return 使用int类型接收返回值
     */
    int insertBatch(java.util.List<Table1> list);

    /**
     *  批量保存数据对象
     * @param list 需要保存的对象集合
     * @return 使用long类型接收返回值
     */
    long insertBatch(java.util.List<Table1> list);
}
```

## INSERT语句知识点
```sql
insert into table(a,b,c) value ('','','');

insert into table(a,b,c) values ('','','');
```

## 打印mybatis的执行sql
> 参考地址：https://www.cnblogs.com/zhukf/p/12843093.html
```yaml
# logging.level后面拼接上自己的mapper所在的包名
logging:
  level:
    priv:
      xakml:
        demo:
          mysql: debug
```
---
### 测试链接地址  
GET http://localhost:8080/getbyid?id=1  
POST http://localhost:8080/person  
POST http://localhost:8080/test_data  