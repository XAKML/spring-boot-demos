package priv.xakml.demo.mysql;

import lombok.ToString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import priv.xakml.demo.mysql.service.impl.PersonService;
import priv.xakml.demo.mysql.service.impl.TestService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class Boot15MysqlApplicationTests {

    @Autowired
    private PersonService personService;

    @Autowired
    private TestService testService;

    @Test
    void contextLoads() {
    }


    @Test
    public void getAllPersonTest(){
        List<Person> list = personService.getAll();
        for (Person p : list){
            System.out.println(p.toString());
        }
    }

    /**
     * 测试批量保存，foreach方式
     * 主键为非自增类型（需要自己指定）
     */
    @Test
    public void insertPersonBatchTest(){
        List<Person> list = personService.getAll();
        Optional<Person> max_person = list.stream().max(Comparator.comparing(p->p.getId()));
        if(max_person.isPresent()){
            List<Person> list1 = new ArrayList<>();
            for (int i=0;i< 10;i++){
                Person person = new Person();
                person.setId(max_person.get().getId() + i + 1);
                person.setStatus(0);
                person.setUserName("name" + person.getId());
                list1.add(person);
            }
           boolean saved = this.personService.insertBatch(list1);
            if(saved){
                System.out.println("保存成功");
            }else{
                System.out.println("保存失败");
            }
        }
    }

    //手动启用ExecutorType.BATCH方式insert
    @Test
    public void insertPersonBatchManualTest(){
        List<Person> list = personService.getAll();
        Optional<Person> max_person = list.stream().max(Comparator.comparing(p->p.getId()));
        if(max_person.isPresent()){
            List<Person> list1 = new ArrayList<>();
            for (int i=0;i< 10;i++){
                Person person = new Person();
                person.setId(max_person.get().getId() + i + 1);
                person.setStatus(0);
                person.setUserName("name" + person.getId());
                list1.add(person);
            }
            boolean saved = this.personService.insertBatchManual(list1);
            if(saved){
                System.out.println("保存成功");
            }else{
                System.out.println("保存失败");
            }
        }
    }
    //region 测试批量保存时，是否可以自动填充自增主键

    /**
     * 测试自增主键类型的数据，批量新增时是否会自动填充主键的值
     * @throws InterruptedException
     */
    @Test
    public void insertTestBatchTest() throws InterruptedException {
        List<priv.xakml.demo.mysql.Test> list = new ArrayList<>();
        for (int i=1;i<=10;i++){
            list.add(new priv.xakml.demo.mysql.Test());
            Thread.sleep(500);
        }
       boolean saved = testService.insertBatch(list);
        if(saved){
            System.out.println("保存成功");
            list.forEach(obj-> System.out.println(obj.getId()));
        }else{
            System.out.println("保存失败");
        }
    }
    //endregion
}
