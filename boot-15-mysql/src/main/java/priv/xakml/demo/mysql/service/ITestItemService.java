package priv.xakml.demo.mysql.service;

import priv.xakml.demo.mysql.TestItem;

import java.util.List;

/**
 * @author lhx
 * @date 2021-11-18 16:15
 **/
public interface ITestItemService {

    boolean insertTestItemBatch(List<TestItem> list);
}
