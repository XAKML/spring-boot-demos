package priv.xakml.demo.mysql;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author lhx
 * @date 2021-09-26 13:14
 **/
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(org.springframework.dao.DuplicateKeyException.class)
    public String handleDbUniConstraintException(org.springframework.dao.DuplicateKeyException duplicate_ex){
        return duplicate_ex.getCause().getMessage();
    }
}
