package priv.xakml.demo.mysql;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author lhx
 * @date 2021-11-18 16:16
 *
 * 注解 @Mapper 可以省略， 在启动类上添加 @MapperScan({"priv.xakml.demo.mysql"})， 注解参数使用mapper的包名
 **/
@Mapper
public interface TestMapper {
    List<Test> selectAllTest();


    /**
     * 新建test data
     * @param data
     * @return
     */
    int insertTestData(priv.xakml.demo.mysql.Test data);

    /**
     * 批量保存数据（主键自动生产，自增）测试批量时是否能自动填充自增主键的值
     * @param list
     * @return
     */
    int insertBatch(List<priv.xakml.demo.mysql.Test> list);
}