package priv.xakml.demo.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import priv.xakml.demo.mysql.Exceptions.BizException;
import priv.xakml.demo.mysql.service.IPersonService;
import priv.xakml.demo.mysql.service.ITestService;

/**
 * @author lhx
 * @date 2021-09-25 11:01
 **/
@RestController
public class DefaultController {

    private final IPersonService personService;
    private final ITestService testService;

    @Autowired
    public DefaultController(IPersonService personService,
                             ITestService testService) {
        this.personService = personService;
        this.testService = testService;
    }


    @PostMapping("person")
    public String createPerson(@RequestBody Person person){
        int newId = this.personService.insert(person);
        if(newId > 0){
            return "create success";
        }else{
            return  "create false";
        }
    }

    /**
     * 新建测试数据
     * @return
     */
    @PostMapping("test_data")
    public String createTestData(@RequestBody Test data){
        try {
            boolean isSaved = this.testService.insertTestData(data);
            return String.valueOf(isSaved);
        }catch (BizException biz_ex){
            return "保存失败，操作已回滚：" + biz_ex.getMessage();
        }
    }
    @GetMapping("getbyid")
    public Person getPerson(int id){
        Person p = this.personService.getPersonById(id);
        return p;
    }
}
