package priv.xakml.demo.mysql.service;

import priv.xakml.demo.mysql.Test;

import java.util.List;

/**
 * @author lhx
 * @date 2021-11-18 16:14
 **/
public interface ITestService {

    /**
     * 查询所有的测试数据
     * @return
     */
    List<Test> getAll();

    /**
     * 保存测试数据
     * @param data
     * @return
     */
    boolean insertTestData(priv.xakml.demo.mysql.Test data);
}
