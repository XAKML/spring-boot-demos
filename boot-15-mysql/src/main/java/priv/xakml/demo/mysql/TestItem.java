package priv.xakml.demo.mysql;

/**
 * testItem
 * @author lhx
 * @date 2021-11-18 15:54
 **/
public class TestItem {

    private long id;
    private long testId;
    private String itemName;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
