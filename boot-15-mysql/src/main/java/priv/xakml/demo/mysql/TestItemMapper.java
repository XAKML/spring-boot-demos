package priv.xakml.demo.mysql;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lhx
 * @date 2021-11-18 16:17
 *
 * 注解 @Mapper 可以省略， 在启动类上添加 @MapperScan({"priv.xakml.demo.mysql"})， 注解参数使用mapper的包名
 **/
@Mapper
public interface TestItemMapper {
    List<TestItem> selectByParentId(@Param("parent_id") long test_id);

    /**
     * 批量保存明细
     * @param list
     * @return
     */
    int insertTestItemBatch(List<TestItem> list);
}
