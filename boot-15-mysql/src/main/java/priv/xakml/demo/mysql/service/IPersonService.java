package priv.xakml.demo.mysql.service;

import priv.xakml.demo.mysql.Person;

/**
 * @author lhx
 * @date 2021-09-07 18:02
 **/
public interface IPersonService {
    int insert(Person person);

    Person getPersonById(int pid);
}
