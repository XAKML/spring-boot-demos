package priv.xakml.demo.mysql;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * test
 * @author lhx
 * @date 2021-11-18 15:49
 **/
public class Test {

    private long id;
    private java.sql.Timestamp createtime;
    private List<TestItem> items;

    public Test() {
        this.createtime = new Timestamp(new Date().getTime());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public java.sql.Timestamp getCreatetime() {
        return createtime;
    }

    public void setCreatetime(java.sql.Timestamp createtime) {
        this.createtime = createtime;
    }

    public List<TestItem> getItems() {
        return items;
    }

    public void setItems(List<TestItem> items) {
        this.items = items;
    }
}
