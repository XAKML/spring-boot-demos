package priv.xakml.demo.mysql;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-24 18:37
 **/
@Mapper
public interface PersonMapper {

    /**
     * 单条保存数据
     * @param person
     * @return
     */
    int insertPerson(Person person);

    /**
     * 批量保存
     * @param list
     * @return 使用mybatis 批量插入数据，通常情况下返回值为int或long类型
     */
    long insertPersonBatch(List<Person> list);


    List<Person> getAll();

    Person getPersonById(@Param("pId") int pId);
}
