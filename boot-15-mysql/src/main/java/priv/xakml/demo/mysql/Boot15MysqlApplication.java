package priv.xakml.demo.mysql;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@MapperScan({"priv.xakml.demo.mysql"})
@SpringBootApplication
public class Boot15MysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot15MysqlApplication.class, args);
    }

}
