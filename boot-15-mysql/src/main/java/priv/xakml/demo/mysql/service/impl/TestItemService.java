package priv.xakml.demo.mysql.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import priv.xakml.demo.mysql.TestItem;
import priv.xakml.demo.mysql.TestItemMapper;
import priv.xakml.demo.mysql.service.ITestItemService;
import priv.xakml.demo.mysql.service.ITestService;

import java.util.List;

/**
 * @author lhx
 * @date 2021-11-18 17:36
 **/
public class TestItemService implements ITestItemService {

    private final TestItemMapper testItemMapper;

    @Autowired
    public TestItemService(TestItemMapper testItemMapper) {
        this.testItemMapper = testItemMapper;
    }

    @Override
    public boolean insertTestItemBatch(List<TestItem> list) {
        if (list == null || list.size() == 0) {
            return false;
        }
        int rowsAffect = this.testItemMapper.insertTestItemBatch(list);
        if (rowsAffect > 0) return true;
        else
            return false;
    }


}
