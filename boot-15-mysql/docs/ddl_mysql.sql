use ef_demo;

CREATE TABLE `user` (
                        `id` int(11) NOT NULL,
                        `username` varchar(50) DEFAULT NULL,
                        `status` int(11) NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `uni_name` (`username`) USING HASH COMMENT '人名不能重复'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- auto-generated definition
create table test
(
    id         bigint auto_increment
        primary key,
    createtime timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP

);

-- auto-generated definition
create table test_item
(
    id        int unsigned auto_increment
        primary key,
    test_id   bigint       not null,
    item_name varchar(255) null,
    constraint fk_test_id
        foreign key (test_id) references test (id)
);

create index fk_test_id_idx
    on test_item (test_id);



