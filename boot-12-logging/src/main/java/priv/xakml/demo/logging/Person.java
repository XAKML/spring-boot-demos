package priv.xakml.demo.logging;

import lombok.Data;
import lombok.ToString;

/**
 * @author lhx
 * @date 2022-06-09 19:50
 **/
@Data
@ToString
public class Person {
    private String name;
    private int age;
}
