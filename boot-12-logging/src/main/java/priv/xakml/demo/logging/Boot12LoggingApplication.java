package priv.xakml.demo.logging;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.logging.LogManager;

@Slf4j
@SpringBootApplication
@EnableScheduling
public class Boot12LoggingApplication implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication.run(Boot12LoggingApplication.class, args);
//        SLF4JBridgeHandler.removeHandlersForRootLogger();
//        SLF4JBridgeHandler.install(); //当前位置可以执行，但无用
    }

    @Override
    public void run(String... args) throws Exception {
        log.trace("app running is trace");
        log.debug("app running is debug");
        log.info("app running is info");
        log.warn("app running is warn");
        log.error("app running is error");

        System.out.println(log.getClass().getName());

//        LogManager.getLogManager().reset();
    }
}
