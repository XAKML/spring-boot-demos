package priv.xakml.demo.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lhx
 * @date 2021-09-04 12:35
 **/
@Slf4j
@RestController
@RequestMapping("hello")
public class HelloController {

    @GetMapping("say")
    public String say(String words){
        String say_content = "Say: " + words;
        log.info(say_content);
        log.warn("警告信息：⚠️" + words);
        log.error("错误信息：❌" + words);
        return "Say: " + words;
    }
}
