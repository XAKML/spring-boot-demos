package priv.xakml.demo.logging;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.ServletContextRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Collections;

/**
 * @author lhx
 * @date 2022-06-09 19:35
 **/
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {
    //implements WebMvcConfigurer
    @Bean
    public FilterRegistrationBean loggingFilterRegistration(){
        //定义过滤器， 可参考以下文档
        //https://www.jianshu.com/p/0bb1ca8644a0
        FilterRegistrationBean<ServletContextRequestLoggingFilter> registration = new FilterRegistrationBean<>();
        ServletContextRequestLoggingFilter filter = new ServletContextRequestLoggingFilter();
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(9999);
        filter.setIncludeHeaders(true);
        registration.setFilter(filter);
        registration.setUrlPatterns(Collections.singleton("/*")); //todo: 注意，这里是能定义一个星号，两个星号则会出错（记录不到日志）
        return registration;
    }
}
