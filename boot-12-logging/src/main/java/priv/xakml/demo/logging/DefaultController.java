package priv.xakml.demo.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import sun.plugin2.util.SystemUtil;

import java.lang.management.ManagementFactory;

/**
 * @author lhx
 * @date 2022-06-09 19:47
 **/
@Slf4j
@RestController
@RequestMapping("api/default")
public class DefaultController {

    @GetMapping("say")
    public String say(String words){
        String say_content = "Say: " + words;
        log.info(say_content);
        return say_content;
    }
    @PostMapping
    public String post(Person person){
        return person.toString();
    }

    /**
     * 查询当前java应用程序的进程Id
     * @return
     */
    @PostMapping("process_name")
    public String getProcessName(){
        String name = ManagementFactory.getRuntimeMXBean().getName();
        if(null != name && name.indexOf('@') > 0){
            return name.split("@")[0];
        }else {
            return "-1";
        }
    }
}
