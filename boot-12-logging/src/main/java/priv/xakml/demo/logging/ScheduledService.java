package priv.xakml.demo.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.lang.management.ManagementFactory;

/**
 * @author lhx
 * @date 2022-02-07 16:06
 **/
@Slf4j
@Component
public class ScheduledService {

//    @Scheduled(fixedDelay = 500)
    public void schedule(){

        log.info("fixedRate。这个相对难以理解一些。在理想情况下，下一次开始和上一次开始之间的时间间隔是一定的。但是默认情况下 Spring Boot 定时任务是单线程执行的。当下一轮的任务满足时间策略后任务就会加入队列，也就是说当本次任务开始执行时下一次任务的时间就已经确定了，由于本次任务的“超时”执行，下一次任务的等待时间就会被压缩甚至阻塞，算了画张图就明白了");
    }
}
