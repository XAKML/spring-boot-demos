# spring boot 日志记录测试项目

### Reference Documentation
For further reference, please consider the following sections:
* slf4j : 配置文件，需要在classpath创建logback-spring.xml(非springboot项目可能需要使用logback.xml文件)
* 需要配置输出到控制台或者文件，需要编写不同的appender，然后在logger标签中引用已经写好的appender

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.4/maven-plugin/reference/html/#build-image)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/2.5.4/reference/htmlsingle/#configuration-metadata-annotation-processor)

```xml
<!--file: logback-spring.xml-->
<appender name="Console"
          class="ch.qos.logback.core.ConsoleAppender">
    <layout class="ch.qos.logback.classic.PatternLayout">
        <Pattern>
            %black(%d{ISO8601}) %highlight(%-5level) [%blue(%t)] %yellow(%C{1.}): %msg%n%throwable
        </Pattern>
    </layout>
    <!--        <filter class="ch.qos.logback.classic.filter.LevelFilter">-->
    <!--            <level>trace</level>-->
    <!--            <onMatch>ACCEPT</onMatch>-->
    <!--            <onMismatch>DENY</onMismatch>-->
    <!--        </filter>-->
</appender>
```

参考网址：  
https://www.cnblogs.com/gavincoder/p/10091757.html  
https://www.jianshu.com/p/9e15d91e42c0

官网参考地址：（logger元素）  
https://logback.qos.ch/manual/configuration.html

注意，如果要把日志输出到不同的文件，可以使用logger元素，并指定append-ref子元素

四、总结  
1、输出源选择
logback的配置，需要配置输出源appender，打日志的logger（子节点）和root（根节点），实际上，它输出日志是从子节点开始，子节点如果有输出源直接输入，如果无，判断配置的addtivity，是否向上级传递，即是否向root传递，传递则采用root的输出源，否则不输出日志。

2、日志级别Level
日志记录器（Logger）的行为是分等级的： 分为OFF、FATAL、ERROR、WARN、INFO、DEBUG、ALL或者您定义的级别。
Log4j建议只使用四个级别，优先级从高到低分别是 ERROR、WARN、INFO、DEBUG，优先级高的将被打印出来。（logback通用）
通过定义级别，可以作为应用程序中相应级别的日志信息的开关。
比如在这里定义了INFO级别，则应用程序中所有DEBUG级别的日志信息将不被打印出来。（设置INFO级别，即：>=INFO 生效）
项目上生产环境的时候一定得把debug的日志级别重新调为warn或者更高，避免产生大量日志。


## springboot 应用程序配置
参考文档：  
https://blog.csdn.net/JokerLJG/article/details/123224002

> WebMvcConfigurer配置接口是Spring内部的一种配置方式，采用JavaBean的形式来代替传统的xml配置文件形式进行针对框架个性化定制。接口提供了很多方法让我们来定制SpringMVC的配置。可以用来自定义处理器、拦截器、视图解析器、转换器、设置跨域等。

    SpringBoot1.5版本前都是靠重写WebMvcConfigurerAdapter的方法来添加自定义拦截器，消息转换器等。
    SpringBoot2.0版本后，WebMvcConfigurerAdapter类被标记为@Deprecated。推荐下面两种方式：
        实现WebMvcConfigurer接口（推荐）；
        继承WebMvcConfigurationSupport类。


## external tools 
配置参考地址：   
https://www.jetbrains.com/help/idea/configuring-third-party-tools.html


https://www.logicbig.com/tutorials/misc/java-logging/jul-to-slf4j.html

https://www.logicbig.com/tutorials/misc/java-logging/jul-to-slf4j.html
第二十五章：日志管理之自定义Appender
https://www.cnblogs.com/okong/p/springboot-twenty-five.html


## 定时任务参考资料

https://www.liaoxuefeng.com/wiki/1252599548343744/1282385878450210
```java
@Component
public class TaskService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Scheduled(initialDelay = 60_000, fixedRate = 60_000)
    public void checkSystemStatusEveryMinute() {
        logger.info("Start check system status...");
    }
}
```
> 上述注解指定了启动延迟60秒，并以60秒的间隔执行任务。现在，我们直接运行应用程序，就可以在控制台看到定时任务打印的日志：  

```shell
2020-06-03 18:47:32 INFO  [pool-1-thread-1] c.i.learnjava.service.TaskService - Start check system status...
2020-06-03 18:48:32 INFO  [pool-1-thread-1] c.i.learnjava.service.TaskService - Start check system status...
2020-06-03 18:49:32 INFO  [pool-1-thread-1] c.i.learnjava.service.TaskService - Start check system status...
```

fixedRate和fixedDelay的区别：

fixedDelay非常好理解，它的间隔时间是根据上次的任务结束的时候开始计时的。  
比如一个方法上设置了fixedDelay=5*1000，那么当该方法某一次执行结束后，开始计算时间，当时间达到5秒，就开始再次执行该方法。

fixedRate理解起来比较麻烦，它的间隔时间是根据上次任务开始的时候计时的。  
比如当方法上设置了fiexdRate=5*1000，该执行该方法所花的时间是2秒，那么3秒后就会再次执行该方法。
但是这里有个坑，当任务执行时长超过设置的间隔时长，那会是什么结果呢?  
打个比方，比如一个任务本来只需要花2秒就能执行完成，我所设置的fixedRate=5*1000，但是因为网络问题导致这个任务花了7秒才执行完成。  
当任务开始时Spring就会给这个任务计时，5秒钟时候Spring就会再次调用这个任务，可是发现原来的任务还在执行，
这个时候第二个任务就阻塞了（这里只考虑单线程的情况下，多线程后面再讲），甚至如果第一个任务花费的时间过长，还可能会使第三第四个任务被阻塞。
被阻塞的任务就像排队的人一样，一旦前一个任务没了，它就立马执行。
————————————————
版权声明：本文为CSDN博主「荒野大码农」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/czx2018/article/details/83501945