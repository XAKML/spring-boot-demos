package priv.xakml.demo.cache.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import priv.xakml.demo.cache.model.Person;

/**
 * @author lhx
 * @date 2021-08-04 14:27
 **/
@FeignClient(url = "http://localhost:8080/default",name = "boot-07-cache")
public interface FeignCacheDefaultService {
    //@Cacheable(value = "default",key = "#id")
    @GetMapping("get")
    Person get(@RequestParam int id);

    @PostMapping("new")
    Person push(Person person);
}
