package priv.xakml.demo.activeprofile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.xakml.demo.activeprofile.model.Person;

/**
 * @author lhx
 * @date 2021-08-02 14:58
 **/
@RestController
@RequestMapping("profile")
public class ActiveProfileController {
    private  final Person person;

    @Autowired
    public ActiveProfileController(Person person) {
        this.person = person;
    }

    @GetMapping("person")
    public Person getPerson(){
        return this.person;
    }

}
