package priv.xakml.demo.activeprofile.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lhx
 * @date 2021-08-02 14:49
 **/
@Data
@ToString
@Component
@ConfigurationProperties(prefix = "person")
public class Person {
    private long id;
    private String name;
}
