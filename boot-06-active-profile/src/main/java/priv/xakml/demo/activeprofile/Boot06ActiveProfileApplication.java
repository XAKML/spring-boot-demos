package priv.xakml.demo.activeprofile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class Boot06ActiveProfileApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot06ActiveProfileApplication.class, args);
    }

}
