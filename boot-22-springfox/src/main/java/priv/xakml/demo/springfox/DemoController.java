/***
 * @Api：用在controller类，描述API接口
 * @ApiOperation：描述接口方法
 * @ApiModel：描述对象
 * @ApiModelProperty：描述对象属性
 * @ApiImplicitParams：描述接口参数
 * @ApiResponses：描述接口响应
 * @ApiIgnore：忽略接口方法
 */
package priv.xakml.demo.springfox;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author lhx
 * @date 2022-05-23 18:23
 **/
@RestController
@EnableOpenApi
@Api(value="模式示例控制器",description = "接口描述",tags = "biaoqian")
@RequestMapping("api/demo")
public class DemoController {

    /**
     * 删除指定id的记录
     * @param id 主键Id
     * @return
     */
    @ApiOperation(value = "根据Id删除记录")
    @DeleteMapping
    public boolean removeById(@ApiParam(name = "id",value = "记录主键Id",required = true)
                                      long id){
        if(id % 2 == 0)
            return true;
        else
            return false;
    }
}
