package priv.xakml.demo.boot20scheduling;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author lhx
 * @date 2022-02-07 15:44
 **/
@Slf4j
@Component
public class ScheduledService {

    //springboot定时任务基础常识总结
    //单实例：即每次运行都是使用同一个实例化对象
    //@Scheduled 注解使用的参数可以使用final修饰过的成员变量替代
    //

    /**
     *
     */
    private static long staticAccessTimes = 0;

    private long accessTimes = 0;

    private static long newInstanceTimes = 0;

    private static Date lastExecutionTime = new Date();

    private final long default_delay = 2000;

    public ScheduledService() {
        newInstanceTimes++;
    }

//    @Scheduled(cron = "0/5 * * * * *")
    public void scheduled(){
        log.info("=====>>>>>使用cron  {}",System.currentTimeMillis());
    }

   // @Scheduled(fixedRate = 5000)
    public void scheduled1() {
        log.info("=====>>>>>使用fixedRate{}", System.currentTimeMillis());
    }

    @Scheduled(fixedDelay = default_delay)
    public void scheduled2() {
        lastExecutionTime = new Date();
        log.info("====={}>>>>>fixedDelay{}",ScheduledService.newInstanceTimes,System.currentTimeMillis());
    }
}
