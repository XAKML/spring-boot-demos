package priv.xakml.demo.boot20scheduling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class Boot20SchedulingApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot20SchedulingApplication.class, args);
    }

}
