package priv.xakml.demo.nacosconsumer02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"priv.xakml.demo.nacosconsumer02"})
@SpringBootApplication
public class Boot08NacosConsumer02Application {

    public static void main(String[] args) {
        SpringApplication.run(Boot08NacosConsumer02Application.class, args);
    }

}
