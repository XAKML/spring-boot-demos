package priv.xakml.demo.nacosconsumer02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lhx
 * @date 2021-08-20 14:35
 **/
@RestController
@RequestMapping("nacos")
public class HelloController {

    final
    IHelloNacosService helloNacosService;

    @Autowired
    public HelloController(IHelloNacosService helloNacosService) {
        this.helloNacosService = helloNacosService;
    }

    @GetMapping
    @ResponseBody
    public String hello(){
        String result =this.helloNacosService.Hello();
        return result;
    }
}