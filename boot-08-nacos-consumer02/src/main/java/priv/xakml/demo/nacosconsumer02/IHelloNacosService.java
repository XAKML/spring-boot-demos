package priv.xakml.demo.nacosconsumer02;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author lhx
 * @date 2021-08-20 15:00
 **/
@FeignClient(name = "boot-08-nacos")
public interface IHelloNacosService {
    @GetMapping("/a/hello")
    String Hello();
}
