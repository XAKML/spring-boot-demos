package priv.xakml.demo.valid.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author lhx
 * @date 2021-05-25 10:03
 **/
@Data
@ToString
@Component
@ConfigurationProperties(prefix = "person")
public class Person {

    /**
     * id
     */
    private long id;

    /**
     * 名称
     */
    @NotEmpty(message = "不允许为empty")
    @NotNull(message = "不允许为null")
    private String name;

    /**
     * 地址
     */
//    @Valid
//    @NotNull(message = "地址信息不允许为空")
    //private Address address;


    public Person() {
    }

    /**
     *
     * @param id id
     * @param name 名称
     */
    public Person(long id, String name) {
        this.id = id;
        this.name = name;
    }

//    @Override
//    public String toString() {
//        return "Person{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                '}';
//    }
}
