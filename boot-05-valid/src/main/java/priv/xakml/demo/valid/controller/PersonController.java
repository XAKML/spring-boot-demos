package priv.xakml.demo.valid.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import priv.xakml.demo.valid.model.Person;

import java.util.Date;

/**
 * @author lhx
 * @date 2021-05-25 10:11
 **/
@RestController
@RequestMapping("person")
public class PersonController {

    @GetMapping("/{id}")
    public String get(@PathVariable int id){
        if(id > 0){
            return new Person(id,String.valueOf(new Date().getTime())).toString();
        }
        else
            return "invalid id";
    }

    @PostMapping("post")
    public String Post(@Validated @RequestBody Person obj){
        if(obj != null)
            return  obj.toString();
        else {
            return "obj is null";
        }
    }
}
