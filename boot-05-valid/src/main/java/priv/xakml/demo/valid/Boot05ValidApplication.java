package priv.xakml.demo.valid;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot05ValidApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot05ValidApplication.class, args);
    }

    public void run(String... args) throws Exception {
    }
}
