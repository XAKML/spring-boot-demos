package priv.xakml.demo.valid;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.io.UnsupportedEncodingException;

/**
 * @author lhx
 * @date 2021-05-25 11:32
 **/
public final  class CommonUtil {
    /**
     * 获取参数验证的结果
     *
     * @param bindingResult 参数验证的绑定结果
     * @return 验证结果
     */
    public static String getErrorMessage(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            StringBuilder sb = new StringBuilder();
            for (ObjectError error : bindingResult.getAllErrors()) {
//                try {
//                    sb.append(new String(error.getDefaultMessage().getBytes("ISO8859-1"), "UTF-8") + ";");
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                    return null;
//                }
                sb.append(error.getDefaultMessage());
            }
            return sb.toString();
        }
        return null;
    }
//    原文链接：https://blog.csdn.net/yzh_1346983557/article/details/90265913
}
