package priv.xakml.demo.valid.model;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * @author lhx
 * @date 2021-05-25 10:30
 **/
@Data
@ToString
public class Address {

    /**
     * id
     */
    private int id;

    /**
     * 国家名称
     */
    @NotNull(message = "国家字段不允许为空")
    private String countryName;

    /**
     * 省份名称
     */
    @NotNull(message = "省份字段不允许为空")
    private String provinceName;

    /**
     * 城市名称
     */
    @NotNull(message = "城市字段不允许为空")
    private String cityName;

    /**
     * 街道名称
     */
    @NotNull(message = "街道字段不允许为空")
    private String streetName;

    /**
     * 邮编
     */
    private String zipCode;
}
