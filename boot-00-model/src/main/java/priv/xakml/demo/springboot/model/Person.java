package priv.xakml.demo.springboot.model;

import lombok.Data;

/**
 *
 */
@Data
public class Person {
    private int id;
    private String userName;
    private int status;
}
