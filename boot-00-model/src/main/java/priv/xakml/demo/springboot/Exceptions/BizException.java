package priv.xakml.demo.springboot.Exceptions;

/**
 * @author lhx
 * @date 2021-11-18 17:25
 **/
public class BizException extends RuntimeException{
    public BizException() {
    }

    public BizException(String message) {
        super(message);
    }
}
