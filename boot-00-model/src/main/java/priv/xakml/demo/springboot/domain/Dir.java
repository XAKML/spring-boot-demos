package priv.xakml.demo.springboot.domain;

public class Dir {

    private long id;
    private String dirPath;
    private String dirPathHash;
    private long filesCount;
    private long subDirCount;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getDirPath() {
        return dirPath;
    }

    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }


    public String getDirPathHash() {
        return dirPathHash;
    }

    public void setDirPathHash(String dirPathHash) {
        this.dirPathHash = dirPathHash;
    }


    public long getFilesCount() {
        return filesCount;
    }

    public void setFilesCount(long filesCount) {
        this.filesCount = filesCount;
    }


    public long getSubDirCount() {
        return subDirCount;
    }

    public void setSubDirCount(long subDirCount) {
        this.subDirCount = subDirCount;
    }

}
