package priv.xakml.demos.ftpserver;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.internal.OSSHeaders;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.StorageClass;
import org.apache.logging.log4j.util.Strings;

import java.io.File;
import java.io.IOException;

/**
 * @author lhx
 * @date 2021-04-13 9:57
 **/
public class AliyunOssToolkit {

//    private String ossImgTmpDic="/root/logs/temp";
//    private String ossDic="pkg_pic/";;
//    private String ossDic_wx="qrcode";
//    private String ossMainBucketName="xakml-bkt-readonly";
//    private String ossEndPoint="oss-cn-beijing.aliyuncs.com";
//    private String ossAccessKeyId="LTAI4FesFa2d9AcqLNXvhQV3";
//    private String ossAccessKeySecret="ajuXLoYIZEULIc86wSEURcjtuiOcfn";
//    private String ossServerIp="oss-cn-beijing.aliyuncs.com";

    public String uploadSimpleStream(File file) throws IOException {
        if(!file.exists())return Strings.EMPTY;
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = NetworkStorageSetting.getOssEndPoint();//"http://oss-cn-hangzhou.aliyuncs.com";
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = NetworkStorageSetting.getOssAccessKeyId();//"LTAI0nSLwPcSq7jY";
        String accessKeySecret = NetworkStorageSetting.getOssAccessKeySecret();//"DrmPnprX71iwRAsVhRcoziXDRWj5O9";
        String bucketName = NetworkStorageSetting.getOssMainBucketName();//"eichong-test";
//        String fileName = CommonUtil.MD5(CommonUtil.getRandomStr());
        String filePath;
        if (NetworkStorageSetting.getOssPkgPicDic().endsWith("/"))
            filePath = NetworkStorageSetting.getOssPkgPicDic() + file.getName();
        else
            filePath = NetworkStorageSetting.getOssPkgPicDic() + "/" + file.getName();


        // 创建OSSClient实例。
        OSS ossClient = null;
        try {
            ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 如果需要上传时设置存储类型与访问权限，请参考以下示例代码。
            // <yourObjectName>表示上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, filePath, file);

            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
            metadata.setObjectAcl(CannedAccessControlList.PublicRead);
            putObjectRequest.setMetadata(metadata);
            ossClient.putObject(putObjectRequest);
            // 上传文件流。
//		InputStream inputStream = file.getInputStream();
//		ossClient.putObject("eichong-test", "car001.png", inputStream);

        } finally {
            if (ossClient != null) {
                // 关闭OSSClient。
                ossClient.shutdown();
            }
        }
        //String path = "https://"+bucketName+"."+"oss-cn-hangzhou.aliyuncs.com"+"/"+filePath;
        String path;
        if (filePath.startsWith("/"))
            path = "https://" + bucketName + "." + NetworkStorageSetting.getOssEndPoint() + filePath;
        else
            path = "https://" + bucketName + "." + NetworkStorageSetting.getOssEndPoint() + "/" + filePath;
        return path;
    }

}
