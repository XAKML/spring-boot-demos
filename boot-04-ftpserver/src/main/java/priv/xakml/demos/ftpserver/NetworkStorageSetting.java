package priv.xakml.demos.ftpserver;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 网络存储服务器配置（目前仅支持阿里云）
 * @author lhx
 * @date 2021-04-15 11:46
 **/
@Component
@ConfigurationProperties(prefix = "network.storage")
public class NetworkStorageSetting {
    /**
     * 本地缓存未知
     */
    private static String ossImgTmpDic = "/root/logs/temp";

    /**
     * oss保存子目录
     */
    private static String ossPkgPicDic = "pkg_pic/";

    /**
     *
     */
    private static String ossMainBucketName = "xakml-bkt-readonly";
    /**
     *
     */
    private static String ossEndPoint = "oss-cn-beijing.aliyuncs.com";
    /**
     *
     */
    private static String ossAccessKeyId = "LTAI4FesFa2d9AcqLNXvhQV3";

    /**
     *
     */
    private static String ossAccessKeySecret = "ajuXLoYIZEULIc86wSEURcjtuiOcfn";

    /**
     * 阿里云oss服务节点网址
     */
    private static String ossServerIp = "oss-cn-beijing.aliyuncs.com";

    public static String getOssImgTmpDic() {
        return ossImgTmpDic;
    }

    public void setOssImgTmpDic(String ossImgTmpDic) {
        NetworkStorageSetting.ossImgTmpDic = ossImgTmpDic;
    }

    public static String getOssPkgPicDic() {
        return ossPkgPicDic;
    }

    public void setOssDic(String ossDic) {
        NetworkStorageSetting.ossPkgPicDic = ossDic;
    }

    public static String getOssMainBucketName() {
        return ossMainBucketName;
    }

    public void setOssMainBucketName(String ossMainBucketName) {
        NetworkStorageSetting.ossMainBucketName = ossMainBucketName;
    }

    public static String getOssEndPoint() {
        return ossEndPoint;
    }

    public void setOssEndPoint(String ossEndPoint) {
        NetworkStorageSetting.ossEndPoint = ossEndPoint;
    }

    public static String getOssAccessKeyId() {
        return ossAccessKeyId;
    }

    public void setOssAccessKeyId(String ossAccessKeyId) {
        NetworkStorageSetting.ossAccessKeyId = ossAccessKeyId;
    }

    public static String getOssAccessKeySecret() {
        return ossAccessKeySecret;
    }

    public void setOssAccessKeySecret(String ossAccessKeySecret) {
        NetworkStorageSetting.ossAccessKeySecret = ossAccessKeySecret;
    }

    public static String getOssServerIp() {
        return ossServerIp;
    }

    public void setOssServerIp(String ossServerIp) {
        NetworkStorageSetting.ossServerIp = ossServerIp;
    }
}
