package priv.xakml.demos.ftpserver;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lhx
 * @date 2021-04-15 11:44
 **/
@Component
@ConfigurationProperties(prefix = "ftp.server")
public class FtpServerSetting {
    /**
     * 主目录
     */
    private static String homeDirectory;

    /**
     * 数据端口
     */
    private static int bindingPort;

    /**
     * 被动模式使用的端口范围xxx-xxx模式
     */
    private static String passivePorts;

    private static String passiveExternalAddress;

    public static String getHomeDirectory() {
        return homeDirectory;
    }

    public void setHomeDirectory(String homeDirectory) {
        FtpServerSetting.homeDirectory = homeDirectory;
    }

    public static int getBindingPort() {
        return bindingPort;
    }

    public void setBindingPort(int bindingPort) {
        FtpServerSetting.bindingPort = bindingPort;
    }

    public static String getPassivePorts() {
        return passivePorts;
    }

    public void setPassivePorts(String passivePorts) {
        FtpServerSetting.passivePorts = passivePorts;
    }

    public static String getPassiveExternalAddress() {
        return passiveExternalAddress;
    }

    public void setPassiveExternalAddress(String passiveExternalAddress) {
        FtpServerSetting.passiveExternalAddress = passiveExternalAddress;
    }
}
