package priv.xakml.demo.autowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.xakml.demo.autowired.api.IService;

/**
 * @author lhx
 * @date 2021-08-09 18:31
 **/
@RestController
@RequestMapping("home")
public class HomeController {

   // @Autowired

    IService serviceOne;

    @Autowired
    public HomeController(@Qualifier("two") IService serviceOne) {
        this.serviceOne = serviceOne;
    }
    @GetMapping
    public String hello(String msg){
        return this.serviceOne.hello(msg);
    }

}
