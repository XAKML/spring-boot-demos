package priv.xakml.demo.autowired.impl;

import org.springframework.stereotype.Component;
import priv.xakml.demo.autowired.api.IService;

/**
 * @author lhx
 * @date 2021-08-09 18:29
 **/
@Component("one")
public class ServiceOne implements IService {
    @Override
    public String hello(String msg) {
        return "ServiceOne-->" + msg;
    }
}
