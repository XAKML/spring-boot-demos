package priv.xakml.demo.autowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import priv.xakml.demo.autowired.api.IService;

@SpringBootApplication
public class Boot09AutowiredApplication implements CommandLineRunner {

    @Autowired
    @Qualifier("two")
    IService serviceOne; //这种写法可以装配成功

    public static void main(String[] args) {
        SpringApplication.run(Boot09AutowiredApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if(this.serviceOne != null)
            System.out.println("auto wired success");
    }
}
