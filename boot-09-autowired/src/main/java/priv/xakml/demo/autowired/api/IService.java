package priv.xakml.demo.autowired.api;

/**
 * @author lhx
 * @date 2021-08-09 18:28
 **/
public interface IService {

    String hello(String msg);
}
