package priv.xakml.demo.autowired.impl;

import org.springframework.stereotype.Component;
import priv.xakml.demo.autowired.api.IService;

/**
 * @author lhx
 * @date 2021-08-09 18:30
 **/
@Component("two")
public class ServiceTwo implements IService {
    private  static  int counter = 1;

    public ServiceTwo() {
        counter ++;
    }

    @Override
    public String hello(String msg) {
        return "ServiceTwo-->" +String.valueOf(counter) + "-->" + msg;
    }
}
