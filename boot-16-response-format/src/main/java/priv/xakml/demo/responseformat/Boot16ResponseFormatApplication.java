package priv.xakml.demo.responseformat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot16ResponseFormatApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot16ResponseFormatApplication.class, args);
    }

}
