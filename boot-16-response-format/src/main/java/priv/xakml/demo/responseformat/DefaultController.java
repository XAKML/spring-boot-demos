package priv.xakml.demo.responseformat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

/**
 * @author lhx
 * @date 2021-10-09 17:14
 **/
@RestController
@RequestMapping("api/default")
public class DefaultController {

    //consumes = {"application/json","application/xml"},produces = {"application/json","application/xml"}
    @GetMapping(value = "person")
    public Person getPerson(){
        return new Person(1,"jack",19, new Date());
    }

    /**
     * 序列化数据对象为json格式， 并添加日期格式的格式化模式
     * @return
     */
    @GetMapping(value="person/string")
    public String getPersonString(){
        Person person =
                new Person(1, "jack", 19, new Date());
        String jsonData = null;
        try {
            jsonData = new JsonMapper()
                   // .setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                    .writeValueAsString(person);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonData;

    }
}
