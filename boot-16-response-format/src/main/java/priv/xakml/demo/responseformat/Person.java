package priv.xakml.demo.responseformat;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.ToString;

import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

/**
 * @author lhx
 * @date 2021-10-09 17:15
 **/
@ToString
@JacksonXmlRootElement(localName = "response")
public class Person {
    int id;
    String name;
    int age;
    Date birthday;

    public Person() {
    }

    public Person(int id, String name, int age,Date birthday) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }

    public int getId() {
        return id;
    }

    @XmlElement(name = "id")
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    @XmlElement(name = "age")
    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
