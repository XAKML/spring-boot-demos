package priv.xakml.demo.beaninjection;

import lombok.val;
import lombok.var;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot17BeanInjectionApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(Boot17BeanInjectionApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {


    }
}
