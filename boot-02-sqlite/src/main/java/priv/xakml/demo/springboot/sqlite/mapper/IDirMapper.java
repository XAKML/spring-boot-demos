package priv.xakml.demo.springboot.sqlite.mapper;

import org.apache.ibatis.annotations.Mapper;
import priv.xakml.demo.springboot.sqlite.domain.Dir;

/**
 * @author lhx
 * @date 2021-03-01 14:51
 **/
@Mapper
public interface IDirMapper {
    int createDir(Dir dir);

    /**
     *
     * @param id
     * @return
     */
    Dir selectById(long id);
}
