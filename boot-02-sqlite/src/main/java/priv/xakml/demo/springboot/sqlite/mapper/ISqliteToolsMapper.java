package priv.xakml.demo.springboot.sqlite.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;

/**
 * @author lhx
 * @date 2021-03-02 17:10
 **/
@Mapper
public interface ISqliteToolsMapper {

    /**
     * 校验数据表是否存在
     * @param table_name
     * @return
     */
    long tableExists(String table_name);

    /**
     * 新建dir表
     * @return 影响到的行数
     * @throws SQLException
     * @throws ClassNotFoundException
     */

    void createDirTable();
}
