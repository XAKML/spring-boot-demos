package priv.xakml.demo.springboot.sqlite.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import priv.xakml.demo.springboot.sqlite.domain.Dir;
import priv.xakml.demo.springboot.sqlite.mapper.IDirMapper;
import priv.xakml.demo.springboot.sqlite.service.IDirService;

/**
 * @author lhx
 * @date 2021-03-01 14:56
 **/
@Service
public class DirService implements IDirService {

    private final IDirMapper dirMapper;

    public DirService(IDirMapper dirMapper) {
        this.dirMapper = dirMapper;
    }

    @Override
    public boolean createDir(Dir dir) {
        int rowsAffect = this.dirMapper.createDir(dir);
        return  rowsAffect > 0;
    }

    @Override
    public Dir selectById(long id) {
        return this.dirMapper.selectById(id);
    }
}
