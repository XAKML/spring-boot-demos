package priv.xakml.demo.springboot.sqlite;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.util.Strings;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;
import priv.xakml.demo.springboot.sqlite.domain.Dir;
import priv.xakml.demo.springboot.sqlite.service.IDirService;
import priv.xakml.demo.springboot.sqlite.service.ISqliteToolsService;

import java.io.File;
import java.util.Arrays;

@SpringBootApplication
public class Boot02SqliteApplication implements CommandLineRunner {

    //sqlite + mybatis + springboot 集成方案，参考文档
    //https://www.cnblogs.com/desireyang/p/13234985.html
    //https://www.cnblogs.com/zblwyj/p/10668803.html
    //https://www.jianshu.com/p/382adbbc6292

    //datasource.url 格式参考资料
    //https://my.oschina.net/u/4401288/blog/3582963

    private final IDirService dirService;
    private final ISqliteToolsService sqliteToolsService;

    public Boot02SqliteApplication(IDirService dirService,ISqliteToolsService sqliteToolsService) {
        this.dirService = dirService;
        this.sqliteToolsService = sqliteToolsService;
    }


    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        //spring boot 2.x 之后，代码方式禁用绑定端口的代码片段
//        new SpringApplicationBuilder(Boot02SqliteApplication.class)
//                .web(WebApplicationType.NONE)
//                .run(args);

        SpringApplication.run(Boot02SqliteApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Arrays.stream(args).forEach(arg->{
            if(arg!= null && arg.length() > 0)
                System.out.println(arg);
        });
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        //$ java -jar App.jar -h
        options.addOption("datasource", "spring.datasoruce.url", false, "sqlite文件路径");
        CommandLine commandLine = parser.parse(options, args);

            String db_file = commandLine.getOptionValue("spring.datasoruce.url");
            System.out.println("sqlite file = "+db_file);
        System.out.println("正在检查运行环境...");
//        dbInit();
        System.out.println("运行环境检查完毕");
//        Dir d = getDir(1);
//        System.out.println(d.getDirPath());
//        String home_path = getJarFilePath();
//        System.out.println(home_path);
    }

    private boolean createDir(){
        Dir dir = new Dir();
        dir.setDirPath("/program");
        dir.setDirPathHash("01901820912i3912kj");
        dir.setFilesCount(10);
        dir.setSubDirCount(2);
        boolean created = this.dirService.createDir(dir);
        return  created;

    }

    private Dir getDir(long id){
        Dir dir = this.dirService.selectById(id);
        return  dir;
    }

    private void dbInit(){
        this.sqliteToolsService.createDirTable();
    }

    private String getJarFilePath(){
        try {
            ApplicationHome home = new ApplicationHome();
            File jarFile = home.getSource();
            return jarFile.getParentFile().toString();
        }catch (NullPointerException null_ex){
            System.out.println("请确确认当前应用程序是以打包后的jar文件运行的--null");
            return Strings.EMPTY;
        }
        catch (IllegalStateException illegal_ex){
            System.out.println("请确确认当前应用程序是以打包后的jar文件运行的");
            return Strings.EMPTY;
        }


    }
}


/**
 * spring boot 不占用端口的方式启动
 * https://www.cnblogs.com/angryprogrammer/p/11757302.html
 * https://www.cnblogs.com/chywx/p/11234527.html
 *
 * 动态设置配置文件
 * https://www.cnblogs.com/fishpro/p/spring-boot-study-cfg.html
 *
 *是否输出logo
 * https://blog.csdn.net/u012903200/article/details/85233545
 *
 * 在Springboot中借助Apache Commons CLI开发命令行工具
 * https://blog.csdn.net/qq_20989105/article/details/80252770
 */
