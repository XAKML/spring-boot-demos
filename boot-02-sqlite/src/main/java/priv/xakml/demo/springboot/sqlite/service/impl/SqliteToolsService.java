package priv.xakml.demo.springboot.sqlite.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import priv.xakml.demo.springboot.sqlite.TableNames;
import priv.xakml.demo.springboot.sqlite.mapper.ISqliteToolsMapper;
import priv.xakml.demo.springboot.sqlite.service.ISqliteToolsService;

/**
 * @author lhx
 * @date 2021-03-02 17:18
 **/
@Service
public class SqliteToolsService implements ISqliteToolsService {

    private final ISqliteToolsMapper sqliteToolsMapper;

    public SqliteToolsService(ISqliteToolsMapper sqliteToolsMapper) {
        this.sqliteToolsMapper = sqliteToolsMapper;
    }


    @Override
    public void createDirTable() {
        if(!this.tableExists(TableNames.DirTableName))
            this.sqliteToolsMapper.createDirTable();
    }

    @Override
    public boolean tableExists(String table_name) {
        long count = sqliteToolsMapper.tableExists(table_name);
        return  count > 0;
    }
}
