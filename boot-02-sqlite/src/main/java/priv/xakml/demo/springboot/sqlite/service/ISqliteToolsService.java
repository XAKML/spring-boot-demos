package priv.xakml.demo.springboot.sqlite.service;

/**
 * @author lhx
 * @date 2021-03-02 17:18
 **/
public interface ISqliteToolsService {
    /**
     * 新建dir表
     * @return
     */
    void createDirTable();

    /**
     * 校验数据表是否存在
     * @param table_name
     * @return
     */
    boolean tableExists(String table_name);
}
