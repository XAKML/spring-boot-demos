package priv.xakml.demo.springboot.sqlite.service;

import priv.xakml.demo.springboot.sqlite.domain.Dir;

/**
 * @author lhx
 * @date 2021-03-01 14:56
 **/
public interface IDirService {
    boolean createDir(Dir dir);

    /**
     *
     * @param id
     * @return
     */
    Dir selectById(long id);
}
