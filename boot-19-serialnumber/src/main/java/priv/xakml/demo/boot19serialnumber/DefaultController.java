package priv.xakml.demo.boot19serialnumber;

import com.jcex.serialnumber.SerialNumberClient;
import com.jcex.serialnumber.SerialNumberResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("api/default")
public class DefaultController {

    final SerialNumberProperties serialNumberProperties;
    //SerialNumberClient client = new SerialNumberClient("http://wms.jcextong.com:8092", "AKC6717916033E4B5FA5279F0C1543EBA3", "AS6D37B5DA07A340188412036A5228DA54"); //模拟
    SerialNumberClient client = null;

    @Autowired
    public DefaultController(SerialNumberProperties serialNumberProperties) {
        this.serialNumberProperties = serialNumberProperties;

        this.client = new SerialNumberClient("http://" + this.serialNumberProperties.getHost(),
                this.serialNumberProperties.getAc_key(),
                this.serialNumberProperties.getAc_secret()); //模拟
    }

    /**
     * 可控过期时间的流水号测试接口
     * @param exp 过期时间,单位:分钟;默认位120分钟,等零食
     * @return
     * @throws NoSuchAlgorithmException
     */
    @GetMapping("serialnumber")
    public String getSerialNumber(int exp) throws NoSuchAlgorithmException {
        if(exp <= 0) exp = 120;
        SerialNumberResult result = this.client.getSerialNumber();
        if(result.Success){
            return result.Success + "   " + result.Msg + "   " + result.Data.toString();
        }else {
            return result.Success + "   " + result.Msg;
        }
    }

    @GetMapping("logging")
    public String logging(){
        return  "ksdjkl";
    }
}
