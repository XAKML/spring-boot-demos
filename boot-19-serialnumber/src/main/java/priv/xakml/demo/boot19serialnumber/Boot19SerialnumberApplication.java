package priv.xakml.demo.boot19serialnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot19SerialnumberApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot19SerialnumberApplication.class, args);
    }

}
