package priv.xakml.demo.boot19serialnumber;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "serialnumber")
public class SerialNumberProperties {

    /**
     * 主机名，不需要填写协议头
     */
    private String host;
    /**
     * access key
     */
    private String ac_key;

    /**
     * access profile
     */
    private String ac_secret;
}
