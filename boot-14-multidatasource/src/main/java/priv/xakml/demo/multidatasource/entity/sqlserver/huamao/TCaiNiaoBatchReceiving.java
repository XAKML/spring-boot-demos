package com.sample;


public class TCaiNiaoBatchReceiving {

  private long caiNiaoBatchReceivingId;
  private String batchNo;
  private String truckNumber;
  private String bigBagId;
  private java.sql.Timestamp createTime;


  public long getCaiNiaoBatchReceivingId() {
    return caiNiaoBatchReceivingId;
  }

  public void setCaiNiaoBatchReceivingId(long caiNiaoBatchReceivingId) {
    this.caiNiaoBatchReceivingId = caiNiaoBatchReceivingId;
  }


  public String getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }


  public String getTruckNumber() {
    return truckNumber;
  }

  public void setTruckNumber(String truckNumber) {
    this.truckNumber = truckNumber;
  }


  public String getBigBagId() {
    return bigBagId;
  }

  public void setBigBagId(String bigBagId) {
    this.bigBagId = bigBagId;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }

}
