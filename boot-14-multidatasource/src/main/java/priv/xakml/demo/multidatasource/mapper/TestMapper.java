package priv.xakml.demo.multidatasource.mapper;

import org.apache.ibatis.annotations.Mapper;
import priv.xakml.demo.multidatasource.entity.Test;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 17:51
 **/
@Mapper
public interface TestMapper {
    List<Test> selectAllTest();
}
