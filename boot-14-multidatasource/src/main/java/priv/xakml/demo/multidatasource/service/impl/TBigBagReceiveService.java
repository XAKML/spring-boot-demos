package priv.xakml.demo.multidatasource.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import priv.xakml.demo.multidatasource.entity.Test;
import priv.xakml.demo.multidatasource.entity.sqlserver.huamao.TBigBagReceive;
import priv.xakml.demo.multidatasource.mapper.sqlserver.huamao.ITBigBagReceiveMapper;
import priv.xakml.demo.multidatasource.service.ITBigBagReceiveService;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:02
 **/
@DS("db_sqlserver")
@Service
public class TBigBagReceiveService implements ITBigBagReceiveService {

    private final ITBigBagReceiveMapper bigBagReceiveMapper;

    @Autowired
    public TBigBagReceiveService(ITBigBagReceiveMapper bigBagReceiveMapper) {
        this.bigBagReceiveMapper = bigBagReceiveMapper;
    }

    @Override
    @Transactional
    public List<TBigBagReceive> getAll() {
        return bigBagReceiveMapper.getAll();
    }
}
