package priv.xakml.demo.multidatasource.service;

import priv.xakml.demo.multidatasource.entity.Test;
import priv.xakml.demo.multidatasource.mapper.TestMapper;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:02
 **/
public interface ITestService {
    List<Test> getAll();
}
