package priv.xakml.demo.multidatasource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.xakml.demo.multidatasource.entity.sqlserver.huamao.TBigBagReceive;
import priv.xakml.demo.multidatasource.service.ITBigBagReceiveService;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:51
 **/
@RestController
@RequestMapping("bigbag")
public class BigbagController {

    private final ITBigBagReceiveService bigBagReceiveService;

    @Autowired
    public BigbagController(ITBigBagReceiveService bigBagReceiveService) {
        this.bigBagReceiveService = bigBagReceiveService;
    }

    @GetMapping("all")
    public List<TBigBagReceive> getAll(){
        return bigBagReceiveService.getAll();
    }
}
