package priv.xakml.demo.multidatasource.mapper.sqlserver.huamao;

import org.apache.ibatis.annotations.Mapper;
import priv.xakml.demo.multidatasource.entity.sqlserver.huamao.TBigBagReceive;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:47
 **/
@Mapper
public interface ITBigBagReceiveMapper {
    List<TBigBagReceive> getAll();
}
