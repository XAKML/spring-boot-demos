package priv.xakml.demo.multidatasource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import priv.xakml.demo.multidatasource.entity.Person;
import priv.xakml.demo.multidatasource.entity.Test;
import priv.xakml.demo.multidatasource.service.IPersonService;
import priv.xakml.demo.multidatasource.service.ITestService;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:11
 **/
@RestController
@RequestMapping(value = "test")
public class TestController {

    private final ITestService testService;
    private final IPersonService personService;
    @Autowired
    public TestController(ITestService testService,
                          IPersonService personService) {
        this.testService = testService;
        this.personService = personService;
    }

    @GetMapping("all")
    public List<Test> getAllTest(){
        List<Test> testList = this.testService.getAll();
        return  testList;
    }

    @PostMapping("person")
    public String createPerson(@RequestBody Person person){
        int newId = this.personService.insert(person);
        if(newId > 0){
            return "create success";
        }else{
            return  "create false";
        }
    }
}
