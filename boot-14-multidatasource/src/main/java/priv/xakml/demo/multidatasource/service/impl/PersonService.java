package priv.xakml.demo.multidatasource.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import priv.xakml.demo.multidatasource.entity.Person;
import priv.xakml.demo.multidatasource.entity.Test;
import priv.xakml.demo.multidatasource.mapper.PersonMapper;
import priv.xakml.demo.multidatasource.mapper.TestMapper;
import priv.xakml.demo.multidatasource.service.IPersonService;
import priv.xakml.demo.multidatasource.service.ITestService;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:02
 **/
@Service
public class PersonService implements IPersonService {

    private final PersonMapper personMapper;

    @Autowired
    public PersonService(PersonMapper personMapper) {
        this.personMapper = personMapper;
    }

    public int insert(Person person){
        try {
            return personMapper.insertPerson(person);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  0;
    }
}
