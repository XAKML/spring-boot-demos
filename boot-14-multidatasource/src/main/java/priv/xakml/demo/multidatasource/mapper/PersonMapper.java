package priv.xakml.demo.multidatasource.mapper;

import org.apache.ibatis.annotations.Mapper;
import priv.xakml.demo.multidatasource.entity.Person;

/**
 * @author lhx
 * @date 2021-09-24 18:37
 **/
@Mapper
public interface PersonMapper {
    int insertPerson(Person person);
}
