package priv.xakml.demo.multidatasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

//@Component
@SpringBootApplication
public class Boot14MultidatasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot14MultidatasourceApplication.class, args);
    }

}
