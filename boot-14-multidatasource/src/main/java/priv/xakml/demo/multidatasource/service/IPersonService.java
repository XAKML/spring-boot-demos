package priv.xakml.demo.multidatasource.service;

import priv.xakml.demo.multidatasource.entity.Person;
import priv.xakml.demo.multidatasource.entity.Test;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:02
 **/
public interface IPersonService {
    int insert(Person person);
}
