package priv.xakml.demo.multidatasource.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @author lhx
 * @date 2021-09-24 18:35
 **/
@Data
@ToString
public class Person {
    private int id;
    private String userName;
    private int status;
}
