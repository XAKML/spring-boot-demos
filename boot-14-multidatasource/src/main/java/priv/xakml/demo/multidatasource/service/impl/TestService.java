package priv.xakml.demo.multidatasource.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import priv.xakml.demo.multidatasource.entity.Test;
import priv.xakml.demo.multidatasource.mapper.TestMapper;
import priv.xakml.demo.multidatasource.service.ITestService;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 18:02
 **/
@Service
public class TestService implements ITestService {

    private final TestMapper testMapper;

    @Autowired
    public TestService(TestMapper testMapper) {
        this.testMapper = testMapper;
    }

    public List<Test> getAll(){
        return testMapper.selectAllTest();
    }
}
