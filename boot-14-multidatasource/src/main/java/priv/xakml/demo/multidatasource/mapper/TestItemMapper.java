package priv.xakml.demo.multidatasource.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import priv.xakml.demo.multidatasource.entity.TestItem;

import java.util.List;

/**
 * @author lhx
 * @date 2021-09-07 17:51
 **/
@Mapper
public interface TestItemMapper {
    List<TestItem> selectByParentId(@Param("parent_id") long test_id);
}
