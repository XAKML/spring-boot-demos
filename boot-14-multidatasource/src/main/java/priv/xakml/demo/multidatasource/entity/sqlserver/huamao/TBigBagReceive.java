package priv.xakml.demo.multidatasource.entity.sqlserver.huamao;


public class TBigBagReceive {

  private long bigBagReceiveId;
  private String uldNoBatchNo;
  private String flightId;
  private String awbNo;
  private String bigBagId;
  private String departurePort;
  private String dischargePort;
  private long weight;
  private long labelWeight;
  private String weightUnit;
  private String opCode;
  private String operator;
  private String opLocation;
  private java.sql.Timestamp opTime;
  private String timeZone;
  private String failedReason;
  private long createUserId;
  private String createUserName;
  private java.sql.Timestamp createTime;
  private long updateUserId;
  private String updateUserName;
  private java.sql.Timestamp updateTime;
  private String isDeleted;
  private java.sql.Timestamp deleteTime;
  private long deleteUserId;
  private String deleteUserName;


  public long getBigBagReceiveId() {
    return bigBagReceiveId;
  }

  public void setBigBagReceiveId(long bigBagReceiveId) {
    this.bigBagReceiveId = bigBagReceiveId;
  }


  public String getUldNoBatchNo() {
    return uldNoBatchNo;
  }

  public void setUldNoBatchNo(String uldNoBatchNo) {
    this.uldNoBatchNo = uldNoBatchNo;
  }


  public String getFlightId() {
    return flightId;
  }

  public void setFlightId(String flightId) {
    this.flightId = flightId;
  }


  public String getAwbNo() {
    return awbNo;
  }

  public void setAwbNo(String awbNo) {
    this.awbNo = awbNo;
  }


  public String getBigBagId() {
    return bigBagId;
  }

  public void setBigBagId(String bigBagId) {
    this.bigBagId = bigBagId;
  }


  public String getDeparturePort() {
    return departurePort;
  }

  public void setDeparturePort(String departurePort) {
    this.departurePort = departurePort;
  }


  public String getDischargePort() {
    return dischargePort;
  }

  public void setDischargePort(String dischargePort) {
    this.dischargePort = dischargePort;
  }


  public long getWeight() {
    return weight;
  }

  public void setWeight(long weight) {
    this.weight = weight;
  }


  public long getLabelWeight() {
    return labelWeight;
  }

  public void setLabelWeight(long labelWeight) {
    this.labelWeight = labelWeight;
  }


  public String getWeightUnit() {
    return weightUnit;
  }

  public void setWeightUnit(String weightUnit) {
    this.weightUnit = weightUnit;
  }


  public String getOpCode() {
    return opCode;
  }

  public void setOpCode(String opCode) {
    this.opCode = opCode;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOpLocation() {
    return opLocation;
  }

  public void setOpLocation(String opLocation) {
    this.opLocation = opLocation;
  }


  public java.sql.Timestamp getOpTime() {
    return opTime;
  }

  public void setOpTime(java.sql.Timestamp opTime) {
    this.opTime = opTime;
  }


  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(String timeZone) {
    this.timeZone = timeZone;
  }


  public String getFailedReason() {
    return failedReason;
  }

  public void setFailedReason(String failedReason) {
    this.failedReason = failedReason;
  }


  public long getCreateUserId() {
    return createUserId;
  }

  public void setCreateUserId(long createUserId) {
    this.createUserId = createUserId;
  }


  public String getCreateUserName() {
    return createUserName;
  }

  public void setCreateUserName(String createUserName) {
    this.createUserName = createUserName;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public long getUpdateUserId() {
    return updateUserId;
  }

  public void setUpdateUserId(long updateUserId) {
    this.updateUserId = updateUserId;
  }


  public String getUpdateUserName() {
    return updateUserName;
  }

  public void setUpdateUserName(String updateUserName) {
    this.updateUserName = updateUserName;
  }


  public java.sql.Timestamp getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(java.sql.Timestamp updateTime) {
    this.updateTime = updateTime;
  }


  public String getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(String isDeleted) {
    this.isDeleted = isDeleted;
  }


  public java.sql.Timestamp getDeleteTime() {
    return deleteTime;
  }

  public void setDeleteTime(java.sql.Timestamp deleteTime) {
    this.deleteTime = deleteTime;
  }


  public long getDeleteUserId() {
    return deleteUserId;
  }

  public void setDeleteUserId(long deleteUserId) {
    this.deleteUserId = deleteUserId;
  }


  public String getDeleteUserName() {
    return deleteUserName;
  }

  public void setDeleteUserName(String deleteUserName) {
    this.deleteUserName = deleteUserName;
  }

}
