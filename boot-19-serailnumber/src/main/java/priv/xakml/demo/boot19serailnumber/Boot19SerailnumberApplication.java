package priv.xakml.demo.boot19serailnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot19SerailnumberApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot19SerailnumberApplication.class, args);
    }

}
