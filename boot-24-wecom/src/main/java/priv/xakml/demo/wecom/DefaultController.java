package priv.xakml.demo.wecom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lhx
 * @date 2022-06-10 21:39
 **/
@RestController
@RequestMapping("api/default")
public class DefaultController {

    @Autowired
    private JcexWecomConfig weComConfig;

    @GetMapping("url")
    public String getWecomUrl(){
        return this.weComConfig.getUrl();
    }

    @GetMapping("say")
    public String say(){
        return "hello";
    }

    /**
     *
     * @param msg
     * @return
     */
    @GetMapping("send_msg")
    public String sendText(@RequestParam String msg){

    }
}
