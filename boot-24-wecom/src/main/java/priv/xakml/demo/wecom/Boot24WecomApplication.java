package priv.xakml.demo.wecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@ComponentScan(basePackages = {"com.jcex.wecom","priv.xakml.demo.wecom"})
@SpringBootApplication
public class Boot24WecomApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot24WecomApplication.class, args);
    }

}
