package priv.xakml.demo.wecom;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lhx
 * @date 2022-06-16 14:05
 **/
@Data
@Component
@ConfigurationProperties("jcex.wecom.config")
public class JcexWecomConfig {
    /**
     * 企业微信应用的URL
     */
    private String url;

    /**
     * 用户Id
     */
    private String userCode;

    /**
     * 企业微信接口调用的私钥
     */
    private String secret;
}
