package priv.xakml.demo.boot25shutdown;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot25ShutdownApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot25ShutdownApplication.class, args);
    }

}
