package priv.xakml.demo.boot25shutdown;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lhx
 * @date 2022-06-22 16:47
 **/
@RestController
@RequestMapping("api/shutdown")
public class ShutdownController implements ApplicationContextAware {

    private ApplicationContext context;

    @GetMapping
    public String shutdownContext(){
        if(ShutdownOptions.controllerIsRunning.isRunning()){
            ShutdownOptions.controllerIsRunning.isRunning(false);
            return "接口服务正在运行，请稍后重试";
        }
        else {
            ((ConfigurableApplicationContext) context).close();
            return "application closed";
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}