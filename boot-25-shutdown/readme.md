## springBoot 示例代码1（helloworld）
关于url路由的一些实践结果
1. 注解RequestMapping，GetMapping,PostMapping中，起始位置的斜杠“/"关系不大，
   有无皆可，系统会自动拼接，
2. 可以使用接口的方式定义控制器的url路径，在接口中定义URL Mapping路径，在Controller中定义@RestController注解；可参考此链接[Spring 注解之RestController和@Requestmapping](https://www.cnblogs.com/east7/p/10847713.html)   比如以下代码：
```java
//接口定义
@RequestMapping("hello")
public interface IHelloService {

    @RequestMapping(value = "/hello")
    String handle01();

    @RequestMapping(value = "/get/person")
    String getPerson(long id);

    @RequestMapping(value = "/get/person_obj",method = RequestMethod.GET,produces = MediaType.APPLICATION_XML_VALUE + ";charset=utf-8")
    Person getPersonObject(long id);
}

//控制器（Controller）实现
@RestController
//@RequestMapping("hello")
public class HelloController implements IHelloService {
   
    @Override
    public String handle01() {
        return this.person.toString();
    }

    @Override
    public String getPerson(long id) {
        Person person = new Person();
        person.setId(id);
        person.setName("world");
        return person.toString();
    }
}
```

## 对象拷贝
BeanUtils.copyProperties(object source,Object target);

## 数据库保存（违反唯一索引）

推出前清理资源,参考文档
https://blog.csdn.net/Flyrainfrdata/article/details/106801443

输出springboot应用程序的pid,参考文档  
https://blog.csdn.net/weixin_45492007/article/details/120577597

拦截器 参考资料
https://www.cnblogs.com/Eleven-Liu/p/11030132.html

Shutdown a Spring Boot Application  
https://www.baeldung.com/spring-boot-shutdown

使用代码方式停止springboot项目后,windows服务是否会自动停止