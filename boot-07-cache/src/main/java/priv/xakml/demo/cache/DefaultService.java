package priv.xakml.demo.cache;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import priv.xakml.demo.cache.model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author lhx
 * @date 2021-08-04 10:39
 **/
@Service
public class DefaultService {

    private List<Person> list = new ArrayList<Person>(){{
        add(new Person(1, "Person-1"));
        add(new Person(2, "Person-2"));
        add(new Person(3, "Person-3"));
    }};
    @Cacheable(value = "default",key = "#id")
    public Person getPerson(int id) {
        //Optional<T> 参考链接
        //https://blog.csdn.net/L_Sail/article/details/78868673
        Optional<Person> personOptional = this.list.stream().filter(p -> p.getId() == id).findFirst();
        if (personOptional.isPresent())
            return personOptional.get();
        else
            return null;
    }
}
