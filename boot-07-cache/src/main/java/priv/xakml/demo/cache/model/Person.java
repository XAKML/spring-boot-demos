package priv.xakml.demo.cache.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lhx
 * @date 2021-08-03 13:55
 **/
@Data
@ToString
@Component
@ConfigurationProperties(prefix = "person")
public class Person {

    /**
     * id
     */
    private long id;

    /**
     * 名称
     */
    private String name;


    public Person() {
    }

    /**
     *
     * @param id id
     * @param name 名称
     */
    public Person(long id, String name) {
        this.id = id;
        this.name = name;
    }

//    @Override
//    public String toString() {
//        return "Person{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                '}';
//    }
}