package priv.xakml.demo.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import priv.xakml.demo.cache.api.FeignCacheDefaultService;
import priv.xakml.demo.cache.model.Person;

/**
 * @author lhx
 * @date 2021-08-03 14:04
 **/
@RestController
@RequestMapping("default")

public class DefaultController implements FeignCacheDefaultService {

    final DefaultService defaultService;

    @Autowired
    public DefaultController(DefaultService defaultService) {
        this.defaultService = defaultService;
    }
    //@PathVariable  @RequestParam 区别
    //https://blog.csdn.net/a15028596338/article/details/84976223
    //@Cacheable(value = "default",key = "#id")
    @GetMapping("get")
    public Person get(@RequestParam int id){
        Person p = this.defaultService.getPerson(id);
        return p;
    }

    @Override
    @PostMapping("new")
    public Person push(@RequestBody Person person) {
        if(null != person)
            person.setName("hello");
        else
            person = new Person();
        return person;
    }
}
