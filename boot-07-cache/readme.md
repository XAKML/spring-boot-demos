[参考链接](http://www.voidcn.com/article/p-kjbqhmxx-bps.html)
[参考链接2](http://www.voidcn.com/article/p-mbnypiwo-ha.html)

题外话: Spring 5中已经摘除了对Guava caching的使用，转而使用Caffeine.详见官方信息SPR-13797

本地缓存，之前一直用Guava Cache，最近spring-boot推荐使用Caffeine Cache。
主要的三种本地缓存性能对比：

简单几步，就可以在spring-boot中配置和使用Caffeine Cache：
```xml
<!-- local cache -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-cache</artifactId>
</dependency>
<dependency>
    <groupId>com.github.ben-manes.caffeine</groupId>
    <artifactId>caffeine</artifactId>
</dependency>
```