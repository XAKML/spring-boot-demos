package priv.xakml.demo.interceptor;

import lombok.Data;

/**
 * @author lhx
 * @date 2021-09-06 9:37
 **/
@Data
public class ApiResult {
    private String code;
    private boolean success;
    private String error_msg;
}
