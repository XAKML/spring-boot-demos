package priv.xakml.demo.interceptor;

import lombok.Data;

/**
 * @author lhx
 * @date 2021-09-04 18:11
 **/
@Data
public class RequestObject {

    private String name;
}
