package priv.xakml.demo.interceptor;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.ServletContextRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.Collections;

/**
 * @author lhx
 * @date 2021-09-04 15:45
 **/
@Configuration
public class WebAppConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthHandler()).addPathPatterns("/*");
        super.addInterceptors(registry);
    }


//    @Bean
//    public FilterRegistrationBean loggingFilterRegistration(){
//        //定义过滤器， 可参考以下文档
//        //https://www.jianshu.com/p/0bb1ca8644a0
//        FilterRegistrationBean<LoggingFilter> registration = new FilterRegistrationBean<>();
//        LoggingFilter filter = new LoggingFilter();
//        filter.setIncludePayload(true);
//        filter.setMaxPayloadLength(9999);
//        registration.setFilter(filter);
//        registration.setUrlPatterns(Collections.singleton("/*"));
//        return registration;
//    }


    @Bean
    public FilterRegistrationBean loggingFilterRegistration(){
        //定义过滤器， 可参考以下文档
        //https://www.jianshu.com/p/0bb1ca8644a0
        FilterRegistrationBean<ServletContextRequestLoggingFilter> registration = new FilterRegistrationBean<>();
        ServletContextRequestLoggingFilter filter = new ServletContextRequestLoggingFilter();
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(9999);
        filter.setIncludeHeaders(true);
        registration.setFilter(filter);
        registration.setUrlPatterns(Collections.singleton("/*"));
        return registration;
    }
//        @Bean
//        public CommonsRequestLoggingFilter requestLoggingFilter() {
//        //参考文档： https://www.jdon.com/56856
//            CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
//            loggingFilter.setIncludeClientInfo(true);
//            loggingFilter.setIncludeQueryString(true);
//            loggingFilter.setIncludePayload(true);
//            loggingFilter.setIncludeHeaders(false);
//            return loggingFilter;
//        }
}
