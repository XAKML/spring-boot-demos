package priv.xakml.demo.interceptor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class Boot13InterceptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot13InterceptorApplication.class, args);
    }

}
