package priv.xakml.demo.interceptor;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author lhx
 * @date 2021-09-04 17:00
 **/
@RestController
@RequestMapping("hello")
public class HelloController {


    @PostMapping
    public ApiResult post(@RequestBody RequestObject content){
        ApiResult result = new ApiResult(){{
            setCode("000");
            setSuccess(true);
            setError_msg("no error");
        }};
        return result;
//        return content.toString() + new Date().toString();
    }
}
