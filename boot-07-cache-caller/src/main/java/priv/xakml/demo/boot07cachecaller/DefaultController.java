package priv.xakml.demo.boot07cachecaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import priv.xakml.demo.cache.api.FeignCacheDefaultService;
import priv.xakml.demo.cache.model.Person;

/**
 * @author lhx
 * @date 2021-08-04 14:46
 **/
@RestController
@RequestMapping("person")
public class DefaultController {

    @Autowired
    FeignCacheDefaultService feignCacheDefaultService;


    @GetMapping("get")
    public Person getPerson(@RequestParam int id){
     return feignCacheDefaultService.get(id);
    }

    @PostMapping("new")
    public Person push(@RequestBody Person person){
       return this.feignCacheDefaultService.push(person);
    }
}
