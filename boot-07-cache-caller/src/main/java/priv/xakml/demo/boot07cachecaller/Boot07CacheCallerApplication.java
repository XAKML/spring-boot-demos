package priv.xakml.demo.boot07cachecaller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients(basePackages = {"priv.xakml.demo.cache.api"})
public class Boot07CacheCallerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot07CacheCallerApplication.class, args);
    }

}
