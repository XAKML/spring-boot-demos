package priv.xakml.demo.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "link")
public class LinkConfig {
    /**
     * 私钥
     */
    private String secret;

    /**
     * Url
     */
    private String url;
}