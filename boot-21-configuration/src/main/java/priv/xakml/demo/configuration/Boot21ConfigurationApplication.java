package priv.xakml.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

//@ConfigurationPropertiesScan("priv.xakml.demo.configuration")
//@EnableConfigurationProperties(LinkConfig.class)
//以上两行开关未启用的情况下，配置文件依旧生效
@SpringBootApplication
public class Boot21ConfigurationApplication implements CommandLineRunner {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Boot21ConfigurationApplication.class, args);
        SpringBootContext.setContext(context);

        String profileName = SpringBootContext.getProfile();
        System.out.println("当前配置环境：" + profileName);
        System.out.println("main函数已运行结束");
    }
//    @Autowired
//    private LinkClient linkClient;

    /**
     *  CommandLineRunner 优先于 main方法执行
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        //linkClient.printLinkConfigField();
        System.out.println("run 方法执行结束");
    }
}
