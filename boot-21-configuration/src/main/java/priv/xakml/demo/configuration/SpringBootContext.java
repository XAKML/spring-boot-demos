package priv.xakml.demo.configuration;

import org.springframework.context.ConfigurableApplicationContext;

/**
 * 读取程序当前使用的配置文件
 * @author lhx
 * @date 2022-06-01 16:15
 **/
public class SpringBootContext {
    private static String active_profile = "";
    private static ConfigurableApplicationContext configAppContext;

    /**
     * 查询当前激活的profile
     * @return 若未显式指定profile，则返回空字符串；其他情况返回已经指定的profile名称
     */
    public static  String getProfile(){
        if(active_profile == "") {
            if(null != SpringBootContext.configAppContext) {
                String[] activeProfiles = SpringBootContext.configAppContext.getEnvironment().getActiveProfiles();
                if (activeProfiles == null || activeProfiles.length == 0)
                    active_profile = "";
                else
                    active_profile = activeProfiles[0];
            }else {
                return "<NULL>";
            }
        }
        return active_profile;
    }

    //region  isXXXProfile
    /**
     * 是否默认配置文件
     * @return true：default or “”
     */
    public static boolean isDefaultProfile(){
        if(SpringBootContext.getProfile() == null
                || SpringBootContext.getProfile().equals("")){
            return true;
        }else
            return false;
    }

    /**
     * 是否开发环境(dev or development)
     * @return true：dev or development
     */
    public static boolean isDevelopmentProfile(){
        if(SpringBootContext.getProfile() != null
                && (SpringBootContext.getProfile().equalsIgnoreCase("dev")
                || SpringBootContext.getProfile().equalsIgnoreCase("development"))){
            return true;
        }else
            return false;
    }

    /**
     * 是否测试环境
     * @return true： test
     */
    public static boolean isTestProfile(){
        if(SpringBootContext.getProfile() != null
                && SpringBootContext.getProfile().equalsIgnoreCase("test")){
            return true;
        }else
            return false;
    }

    /**
     * 是否运行在压力测试环境下（stress test）
     * @return true： stest
     */
    public static boolean isStressTestProfile(){
        if(SpringBootContext.getProfile() != null
                && SpringBootContext.getProfile().equalsIgnoreCase("stest")){
            return true;
        }else
            return false;
    }

    /**
     * 是否仿真环境(预发环境)
     * @return true：staging
     */
    public static boolean isStagingProfile(){
        if(SpringBootContext.getProfile() != null
                && SpringBootContext.getProfile().equalsIgnoreCase("staging")){
            return true;
        }else
            return false;
    }

    /**
     * 是否生产环境
     * @return true：pro or production
     */
    public static boolean isProductionProfile(){
        if(SpringBootContext.getProfile() != null
                && (SpringBootContext.getProfile().equalsIgnoreCase("pro")
                || SpringBootContext.getProfile().equalsIgnoreCase("production"))){
            return true;
        }else
            return false;
    }

    //endregion

    /**
     * 设置Spring boot执行上下文
     * @param context
     */
    public static void setContext(ConfigurableApplicationContext context) {
        SpringBootContext.configAppContext = context;
    }
}
