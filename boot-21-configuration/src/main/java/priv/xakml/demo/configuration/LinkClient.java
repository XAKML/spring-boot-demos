package priv.xakml.demo.configuration;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author lhx
 * @date 2022-05-25 10:07
 **/
@Component
public class LinkClient {

    @Resource
    private LinkConfig linkConfig;

    public void printLinkConfigField(){
        System.out.println(linkConfig.getSecret());
    }

    public String getClientUrl(){
        return this.linkConfig.getUrl();
    }
}
