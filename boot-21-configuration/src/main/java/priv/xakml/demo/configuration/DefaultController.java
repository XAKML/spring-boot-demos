package priv.xakml.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lhx
 * @date 2022-06-01 15:00
 **/
@RestController
@RequestMapping("default")
public class DefaultController {

    @Autowired
    private LinkClient client;

    @GetMapping("client_url")
    public String getClientUrl(){
        if(SpringBootContext.getProfile() == "dev"){
            System.out.println("启动了开发环境");
        }else if(SpringBootContext.getProfile() == ""){
            System.out.println("default");
        }else {
            System.out.println(SpringBootContext.getProfile());
        }
        return this.client.getClientUrl();
    }
}
