package priv.xakml.demo.configuration;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;

@SpringBootTest
class Boot21ConfigurationApplicationTests {

    @Resource
    private LinkConfig linkConfig;

    @Test
    void contextLoads() {
    }

    @Test
    public void testConfigurationProperties(){
        System.out.println(this.linkConfig.getUrl());
        System.out.println(this.linkConfig.getSecret());
    }

}
