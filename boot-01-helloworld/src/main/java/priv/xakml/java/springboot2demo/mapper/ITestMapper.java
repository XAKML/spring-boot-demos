package priv.xakml.java.springboot2demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import priv.xakml.java.springboot2demo.domain.Test;

import java.util.List;

@Mapper
public interface ITestMapper {
    /**
     *
     * @param id
     * @return
     */
    Test getById(long id);

    int updateCurrentPoints(List<Test> list);

    int insertTest(Test obj);
}
