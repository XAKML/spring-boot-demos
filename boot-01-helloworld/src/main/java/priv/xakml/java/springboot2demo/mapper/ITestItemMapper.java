package priv.xakml.java.springboot2demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import priv.xakml.java.springboot2demo.domain.TestItem;

/**
 * @author lhx
 * @date 2021-05-28 15:16
 **/
@Mapper
public interface ITestItemMapper {
    /**
     * 新建明细
     * @param item
     * @return
     */
    int insertTestItem(TestItem item);
}
