package priv.xakml.java.springboot2demo.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.ToString;

/**
 * @author lhx
 * @date 2021-08-27 19:51
 **/
@Data
@ToString
@JacksonXmlRootElement(localName = "")
public class PersonDo {
    private String name;
}
