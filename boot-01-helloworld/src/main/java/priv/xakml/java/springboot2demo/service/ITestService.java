package priv.xakml.java.springboot2demo.service;

import priv.xakml.java.springboot2demo.domain.Test;

import java.util.List;

public interface ITestService {
    /**
     * 通过Id查询
     * @param id
     * @return
     */
    Test getTestById(long id);

    boolean updateBatch(List<Test> list);

    /**
     * 新建示例数据记录(包括明细)
     * @param obj
     * @return
     */
    boolean Insert(Test obj)throws Exception;
}
