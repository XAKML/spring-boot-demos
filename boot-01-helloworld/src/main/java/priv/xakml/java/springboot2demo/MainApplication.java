package priv.xakml.java.springboot2demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.web.context.WebServerPortFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder appBuilder = new SpringApplicationBuilder(MainApplication.class);
        appBuilder.build(args).addListeners(writePidToFile());
        appBuilder.run(args);
//        ConfigurableApplicationContext applicationContext = SpringApplication.run(MainApplication.class, args);
//        applicationContext.addApplicationListener(writePidToFile());
    }

    public static ApplicationPidFileWriter writePidToFile(){
        ApplicationPidFileWriter pidFileWriter = new ApplicationPidFileWriter(new File("pid.txt"));
        return pidFileWriter;
    }
    public static WebServerPortFileWriter writePortToFile(){
        WebServerPortFileWriter webServerPortFileWriter = new WebServerPortFileWriter(new File("port.txt"));
        return webServerPortFileWriter;
    }
}
