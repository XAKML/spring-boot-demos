package priv.xakml.java.springboot2demo.domain;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class Test {
    private long id;
    private Timestamp createTime;

    private List<TestItem> items;
    public Test() {
    }

    public Test(long id, Timestamp createTime) {
        this.id = id;
        this.createTime = createTime;
    }
}
