package priv.xakml.java.springboot2demo.destory;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContextListener;

/**
 * @author lhx
 * @date 2022-06-22 16:27
 **/
@Configuration
public class ShutdownHookConfiguration {
    @Bean(destroyMethod = "destory")
    public Bean3 initializeBean3() {
        return new Bean3();
    }

//    @Bean
//    ServletListenerRegistrationBean<ServletContextListener> servletListener() {
//        ServletListenerRegistrationBean<ServletContextListener> srb = new ServletListenerRegistrationBean<>();
//        srb.setListener(new ExampleServletContextListener());
//        return srb;
//    }
}
