package priv.xakml.java.springboot2demo.destory;

import java.io.File;

/**
 * @author lhx
 * @date 2022-06-22 16:26
 **/
public class Bean3 {
    public void destory(){
        System.out.println("Shutdown triggered using bean destroy method.");
        File file = new File("pid.txt");
        if(file.exists()){
            file.delete();
        }
    }
}
