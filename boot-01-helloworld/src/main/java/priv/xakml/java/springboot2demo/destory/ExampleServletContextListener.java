package priv.xakml.java.springboot2demo.destory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author lhx
 * @date 2022-06-22 16:27
 **/
public class ExampleServletContextListener  implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("Shutdown triggered using ServletContextListener.");
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        // Triggers when context initializes
    }

}
