package priv.xakml.java.springboot2demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

/**
 * 测试Controller类型的实例化逻辑
 * 每个请求是使用的是新实力还是单实例
 *  ===========================
 *  测试结论：
 *     controller整个应用程序周期内只实例化一次
 *     静态成员和类型成员作用基本可以通用
 * @author lhx
 * @date 2022-06-23 17:32
 **/
@RestController
@RequestMapping("api/default")
public class DefaultController {

    /**
     *
     */
    private static long staticAccessTimes = 0;

    private long accessTimes = 0;

    private static long newInstanceTimes = 0;

    public DefaultController() {
        newInstanceTimes++;
    }

    @GetMapping("current_time")
    public String getCurrentTime(){
        return SimpleDateFormat.getDateInstance().format(new Date());
    }

    /**
     *
     * @return
     */
    @GetMapping("new_instance_times")
    public long getNewInstanceTimes(){
        return DefaultController.newInstanceTimes;
    }

    /**
     * 查询类成员修改影响范围
     * @return
     */
    @GetMapping("new_access_times")
    public long getAccessTimes(){
        return accessTimes++;
    }

}
