package priv.xakml.java.springboot2demo.domain;

import lombok.Data;
import lombok.ToString;

/**
 * @author lhx
 * @date 2021-05-28 15:14
 **/

@Data
@ToString
public class TestItem {
    /**
     *
     */
    private long id;
    /**
     *
     */
    private long testId;
    /**
     *
     */
    private String testName;

}
