package priv.xakml.java.springboot2demo.api;

import org.springframework.web.bind.annotation.*;
import priv.xakml.java.springboot2demo.domain.Test;
import priv.xakml.java.springboot2demo.model.PagingParam;
import priv.xakml.java.springboot2demo.model.Person;
import priv.xakml.java.springboot2demo.model.PersonQueryParam;

/**
 * Controller中的mapping路径，可以在实现的接口中定义，
 * 用在Controller重写接口中的方法即可
 *
 * @author lhx
 * @date 2021-08-05 15:32
 **/
@RequestMapping("hello")
public interface IHelloService {

    @RequestMapping(value = "/hello")
    String handle01();

    @RequestMapping(value = "/get/person")
    String getPerson(long id);


    Person getPersonObject(long id);

    @PostMapping("/test")
    String createTest(@RequestBody Test test);

    @PostMapping("person/new")
    String createPerson(int id,String name);

    @GetMapping("person/find")
    String findPerson(PersonQueryParam queryParam, PagingParam pagingParam);

    @GetMapping("/update/batch")
    String updateBatch();
}
