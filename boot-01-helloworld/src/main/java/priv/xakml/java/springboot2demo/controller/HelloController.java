package priv.xakml.java.springboot2demo.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import priv.xakml.java.springboot2demo.api.IHelloService;
import priv.xakml.java.springboot2demo.domain.PersonDo;
import priv.xakml.java.springboot2demo.domain.Test;
import priv.xakml.java.springboot2demo.model.PagingParam;
import priv.xakml.java.springboot2demo.model.Person;
import priv.xakml.java.springboot2demo.model.PersonQueryParam;
import priv.xakml.java.springboot2demo.service.ITestService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("hello")
public class HelloController implements IHelloService{
//implements IHelloService
    private final Person person;

    @Autowired
    private ITestService testService;

    @Autowired
    public HelloController(Person person) {
        this.person = person;
    }

    @Override
//    @RequestMapping(value = "/hello")
    public String handle01(){
        return this.person.toString();
    }

    @Override
   // @RequestMapping(value = "/person")
    public String getPerson(long id){
        Person person = new Person();
        person.setId(id);
        person.setName("world");

        PersonDo personDo = new PersonDo();
        BeanUtils.copyProperties(person,personDo);
        return personDo.toString();
    }

    @RequestMapping(value = "/get/person_obj",method = RequestMethod.GET,produces = MediaType.APPLICATION_XML_VALUE + ";charset=utf-8")
    public Person getPersonObject(long id){
        Person person = new Person();
        person.setId(id);
        person.setName("world");

//        PersonDo personDo = new PersonDo();
//        BeanUtils.copyProperties(person,personDo);
        return person;
    }
//    @GetMapping(value = "/test")
    private String getTestObject(long id){
        return testService.getTestById(id).toString();
    }
    @Override
    @PostMapping(value = "test",consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    public String createTest(@RequestBody Test test){
        //springboot 事务的使用说明及示例
        //https://blog.csdn.net/jiangyu1013/article/details/84397366
        //{"createTime":1622191050331,"items":[{"testName":"A"},{"testName":"C"}]}
        try {
            boolean isAdded = testService.Insert(test);
            if(isAdded)
                return "insert succeed";
            else
                return "insert failed";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "execute complete";
    }

    @Override
    public String createPerson(int id, String name) {
        Person person = new Person(){{
            setId(id);
            setName(name);
        }};
        return person.toString();
    }

    @Override
    public String findPerson(PersonQueryParam queryParam, PagingParam pagingParam) {
        Person person = new Person(){{
            setId(1);
            setName("分页多条件查询");
        }};
        return person.toString();
    }

    @Override
//    @GetMapping("/update/batch")
    public String updateBatch(){
        List<Test> list = new ArrayList<>();
        list.add(new Test(1,new Timestamp(new Date().getTime())));
        list.add(new Test(2,new Timestamp(new Date().getTime())));
        list.add(new Test(3,new Timestamp(new Date().getTime())));
        list.add(new Test(4,new Timestamp(new Date().getTime())));


        boolean isOK = this.testService.updateBatch(list);
        if(isOK)
            return  "Ok";
        else
            return  "failed";
    }



}
