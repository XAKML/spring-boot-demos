package priv.xakml.java.springboot2demo.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
public class PagingParam {

    private int pageIndex;
    private int pageSize ;
}
