package priv.xakml.demo.boot26cmdline;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot26CmdlineApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(priv.xakml.demo.boot26cmdline.Boot26CmdlineApplication.class, args);
        System.out.println("springboot application is start up!");
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("CommandLineRunner is executed");
    }
}
