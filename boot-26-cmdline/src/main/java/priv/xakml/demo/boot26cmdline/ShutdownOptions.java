package priv.xakml.demo.boot26cmdline;

import lombok.Getter;

/**
 * @author lhx
 * @date 2022-07-04 9:10
 **/
public class ShutdownOptions {

    /**
     * 控制器正在运行
     */
    public static Option controllerIsRunning;

    static {
        controllerIsRunning = new Option().isRunning(false).message("控制器是否在运行中");
    }
    /**
     *
     */
    @Getter
    public static class Option {

        /**
         * 选项开关
         */
        private boolean isRunning = false;

        /**
         * 选项提示文字
         */
        private String optionMessage;

        public Option isRunning(boolean running){
            this.isRunning = running;
            return this;
        }

        public Option message(String msg){
            this.optionMessage = msg;
            return this;
        }
    }

}
