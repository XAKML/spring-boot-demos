package priv.xakml.demo.boot26cmdline;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 测试结果：
 * springboot框架中， controller是单例模式，controller中成员变量将影响控制器的所有生命周期，类似于静态成员
 *
 *
 */

/**
 * @author lhx
 * @date 2022-06-22 16:47
 **/
@RestController
@RequestMapping("api/default")
public class DefaultController {

    private String msg = "hello";
    private int currentValue = 0;




    /**
     * execute a long time process
     * @return
     */
    @GetMapping("run_longtime")
    public String runlongTime() throws InterruptedException {
        ShutdownOptions.controllerIsRunning.isRunning(true);
        TimeUnit.SECONDS.sleep(20);
        ShutdownOptions.controllerIsRunning.isRunning(false);

        return SimpleDateFormat.getDateInstance().format(new Date());
    }
    @GetMapping("hello")
    public String hello(){
        return "hello world";
    }

    @GetMapping("value_init")
    public String initFieldValue(){
        this.msg = "hello" + SimpleDateFormat.getDateInstance().format(new Date());
        this.currentValue = new Random().nextInt();
        return msg + "\r\n" + currentValue;
    }

    @GetMapping("value_get")
    public String getFieldValue(){
        return msg + "\r\n" + currentValue;
    }
}
