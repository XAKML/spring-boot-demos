package priv.xakml.demo.springboot.redis;

import lombok.Data;

/**
 * @author lhx
 * @date 2022-05-30 11:53
 **/
@Data
public class User {

    private String name;
    private String email;
    private String pwd;

    public User(String s, String aa, String aa123456) {
        this.name = s;
        this.email = aa;
        this.pwd = aa123456;
    }
}
