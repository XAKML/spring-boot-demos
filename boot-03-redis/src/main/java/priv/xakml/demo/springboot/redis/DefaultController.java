package priv.xakml.demo.springboot.redis;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lhx
 * @date 2022-05-30 11:50
 **/
@RestController
@RequestMapping("default")
public class DefaultController {
//    @RequestMapping("getUser")
//    @Cacheable(value="user-key")
//    public User getUser() {
//        User user=new User("aa@126.com", "aa", "aa123456");
//        System.out.println("若下面没出现“无缓存的时候调用”字样且能打印出数据表示测试成功");
//        return user;
//    }
    @GetMapping("hello")
    public String hello(@RequestParam String words){
        return  "hello" + " " + words;
    }
}
