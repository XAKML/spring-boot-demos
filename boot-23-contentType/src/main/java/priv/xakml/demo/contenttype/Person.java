package priv.xakml.demo.contenttype;

import lombok.Data;
import lombok.ToString;

/**
 * @author lhx
 * @date 2022-06-01 10:36
 **/
@Data
@ToString
public class Person {
    private String name;
}
