package priv.xakml.demo.contenttype;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.File;
import java.io.IOException;

/**
 * @author lhx
 * @date 2022-06-01 10:35
 **/
@RestController
@RequestMapping("default")
public class DefaultController {

    /**
     *  如果服务接口要接收content-type=application/x-www-form-urlencoded;charset=UTF-8的数据类型:
     *      则服务方法中不能@RequestBody注解,否则接收不到任何数据,而且还会提示  Content type  **** not supported
     *  如果控制器方法参数不添加@RequestBody,则默认使用x-www-form-urlencoded可接收数据(PostMapping注解不额外设置 consumes 参数);
     *  如果控制器方法添加注解@RequsetBody,则可使用application/json方式接收数据(PostMapping注解不额外设置 consumes 参数);
     *  如果设置了PostMapping注解的consumes的参数,则只能接收当前类型的请求数据;
     * @param person
     * @return
     */
    @PostMapping(value = "add")
    public String addPerson(@RequestBody Person person){
        return person.toString();
    }

    /**
     * form-urlencoded 表单提交<br />
     * key=value&key2=value2格式
     * http post 表单提交 body内容参考资料：
     * https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Methods/POST
     * @param person
     * @return
     */
    @PostMapping(value="add_by_form_url_encoded")
    public String addPersonByFormUrlEncoded(Person person){
        return person.toString();
    }

    /**
     * 单独上传文件，不包含表单中的其他的非文件字段
     * @param file
     * @return
     */
    @PostMapping("upload")
    public String uploadFile(MultipartFile file){
        if(!file.isEmpty()){
            String fileName = file.getOriginalFilename();
            File file1 = new File(fileName);
            try {
                file.transferTo(file1); //默认存储在： %user home%\AppData\Local\temp\tomcat.8080.xxxx\
                return fileName;
            } catch (IOException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }else{
            return "can't received any file upload";
        }
    }

    /**
     * 表单字段和文件同时接收
     * 参考资料： https://blog.csdn.net/qq_45888242/article/details/121061975
     * https://stackoverflow.com/questions/21329426/spring-mvc-multipart-request-with-json
     * @param person 表单数据
     * @param avatar_file 文件类型封装
     * @return
     */
    @PostMapping("add/person")
    public String uploadFileAndOtherField(@RequestPart("person") Person person, @RequestPart("avatar") MultipartFile avatar_file){
        if(!avatar_file.isEmpty()){
            String fileName = avatar_file.getOriginalFilename();
            File file1 = new File(fileName);
            try {
                avatar_file.transferTo(file1); //默认存储在： %user home%\AppData\Local\temp\tomcat.8080.xxxx\
                return fileName + person.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }else{
            return "can't received any file upload";
        }
    }
}
