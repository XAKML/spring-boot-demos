package priv.xakml.demo.contenttype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot23ContentTypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(Boot23ContentTypeApplication.class, args);
	}

}
