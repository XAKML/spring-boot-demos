package priv.xakml.demo.exceptionhandler;

import com.sun.org.apache.bcel.internal.classfile.Code;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author lhx
 * @date 2021-09-02 18:04
 **/
@RestController
@RequestMapping("hello")
public class HelloController {

    @GetMapping
    public String get(){
        if(true)
            throw new BizException(CodeEnum.METHOD_NOT_ALLOWED);
        return new Date().toString();
    }

    @PostMapping("create")
    public Result post(@RequestBody RequestObject request){
        if(null == request)
            throw new BizException(CodeEnum.REQUEST_DATA_ISNULL);
        Result result = Result.renderErr(CodeEnum.Success).withRemark(request.toString());
        return result;
    }
}
