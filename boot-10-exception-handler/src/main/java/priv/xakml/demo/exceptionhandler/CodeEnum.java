package priv.xakml.demo.exceptionhandler;

import lombok.Data;

/**
 * @author lhx
 * @date 2021-09-02 18:40
 **/
public enum CodeEnum {

    REQUEST_ERR("REQUEST_ERR"),
    METHOD_NOT_ALLOWED("METHOD_NOT_ALLOWED"),
    SERVER_ERR("SERVER_ERR"),
    Success("success"),
    REQUEST_DATA_ISNULL("请求数据不允许为空");
    protected String Message;

    CodeEnum(String msg){
        this.Message = msg;
    }
    public String getMessage(){
        return this.Message;
    }
}
