package priv.xakml.demo.exceptionhandler;

import lombok.Data;

/**
 * @author lhx
 * @date 2021-09-02 18:44
 **/
@Data
public class Result {
    private String Err;
    private String remark;

    public static Result renderErr(CodeEnum err){
        Result result = new Result();
        result.Err = err.getMessage();
        return result;
    }

    public Result withRemark(String remark) {
        this.remark = remark;
        return this;
    }
}
