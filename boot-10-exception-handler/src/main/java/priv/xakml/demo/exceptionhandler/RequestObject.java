package priv.xakml.demo.exceptionhandler;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author lhx
 * @date 2021-09-03 11:28
 **/
@Data
public class RequestObject {

    /**
     * 时间格式统一处理，参考文档
     * https://www.cnblogs.com/nongzihong/p/11138909.html
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    private String code;
}
