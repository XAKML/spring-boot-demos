package priv.xakml.demo.exceptionhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot10ExceptionHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Boot10ExceptionHandlerApplication.class, args);
	}

}
