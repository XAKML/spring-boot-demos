package priv.xakml.demo.boot18mybatisplus;

import cn.hutool.Hutool;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import priv.xakml.demo.boot18mybatisplus.mapper.TestMapper;
import priv.xakml.demo.boot18mybatisplus.pojo.TableMybatisplusAutoid;
import priv.xakml.demo.boot18mybatisplus.service.ITableMybatisplusAutoidService;
import priv.xakml.demo.boot18mybatisplus.service.ITestService;

import java.util.List;

@SpringBootTest
class Boot18MybatisPlusApplicationTests {


    @Autowired
    private ITableMybatisplusAutoidService tableMybatisplusAutoidService;



    @Test
    void contextLoads() {
    }

    //spring boot 单元测试参考文档
    //https://blog.csdn.net/m0_63951142/article/details/131632392

    @Test
    public void testInsertTableAutoId(){
        TableMybatisplusAutoid tableEntry = new TableMybatisplusAutoid();
        tableEntry.setContent("插入数据时间：" + "@TableId(type = IdType.AUTO)");
        boolean saved = this.tableMybatisplusAutoidService.save(tableEntry);
        System.out.println("new record primary id value = " +tableEntry.getId());
        Assertions.assertTrue(saved);
    }
}
