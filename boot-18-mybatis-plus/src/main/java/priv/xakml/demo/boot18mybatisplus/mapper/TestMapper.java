package priv.xakml.demo.boot18mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import priv.xakml.demo.boot18mybatisplus.model.TestUpdateModel;
import priv.xakml.demo.boot18mybatisplus.pojo.Test;

import java.util.List;


@Mapper
public interface TestMapper extends BaseMapper<Test> {

    int updateLongValueBatch(List<TestUpdateModel> list);

    /**
     * mybatis-plus更新方法，
     * 返回值可以使用int，long类型接收数据
     * @param ids
     * @return
     */
    int batchUpdateLongValueByIds(List<Long> ids);
}
