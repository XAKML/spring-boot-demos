package priv.xakml.demo.boot18mybatisplus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import priv.xakml.demo.boot18mybatisplus.service.ITableMybatisplusAutoidService;
import priv.xakml.demo.boot18mybatisplus.service.impl.TableMybatisplusAutoidService;

//@MapperScan("priv.xakml.demo.boot18mybatisplus.mapper")
@EnableCaching
@SpringBootApplication
@EnableTransactionManagement
public class Boot18MybatisPlusApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Boot18MybatisPlusApplication.class, args);
    }



//    @Autowired
//    private ITableMybatisplusAutoidService tableMybatisplusAutoidService;


    @Override
    public void run(String... args) throws Exception {
//        if(null != tableMybatisplusAutoidService){
//            System.out.println("can use");
//        }else {
//            System.out.println("is null");
//        }

    }
}
