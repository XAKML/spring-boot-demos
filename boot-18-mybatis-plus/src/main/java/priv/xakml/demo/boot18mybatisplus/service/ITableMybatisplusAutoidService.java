package priv.xakml.demo.boot18mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import priv.xakml.demo.boot18mybatisplus.pojo.TableMybatisplusAutoid;
import priv.xakml.demo.boot18mybatisplus.pojo.Test;

public interface ITableMybatisplusAutoidService extends IService<TableMybatisplusAutoid> {
}
