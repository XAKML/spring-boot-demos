package priv.xakml.demo.boot18mybatisplus.controller;

import lombok.Data;
import lombok.ToString;

/**
 * @author lhx
 * @date 2022-05-28 14:21
 **/
@Data
@ToString
public class IpInfo {
    private String ip;
    private long ipLongFormat;
    private String geoLocation;
    private long locationStart;
    private long locationEnd;
}
