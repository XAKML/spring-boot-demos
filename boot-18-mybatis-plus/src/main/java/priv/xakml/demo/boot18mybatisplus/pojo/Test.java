package priv.xakml.demo.boot18mybatisplus.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.Date;

@Data
@ToString
public class Test {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Timestamp createtime;
    private Long longValue;

    public Test() {
        createtime = new Timestamp((new Date()).getTime());
    }
}
