package priv.xakml.demo.boot18mybatisplus.model;

import lombok.Data;
import lombok.ToString;

/**
 * @author lhx
 * @date 2022-05-28 10:06
 **/
@Data
@ToString
public class TestUpdateModel{
    private Long id;
    private Long longValue;

    public TestUpdateModel(Long id, Long longValue) {
        this.id = id;
        this.longValue = longValue;
    }
}
