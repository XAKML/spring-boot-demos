package priv.xakml.demo.boot18mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import priv.xakml.demo.boot18mybatisplus.pojo.TableMybatisplusAutoid;

@Mapper
public interface TableMybatisplusAutoidMapper extends BaseMapper<TableMybatisplusAutoid> {

}
