package priv.xakml.demo.boot18mybatisplus.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.util.Date;

/**
* 测试机在Java项目中，mybatis-plus组件使用autoid类型的测试表。
* @TableName table_mybatisplus_autoid
*/
public class TableMybatisplusAutoid implements Serializable {

    /**
    * 
    */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
    * 编码长度不能超过50
    */
    private String content;

    /**
    * 
    */
    private Date createdTime;

    /**
    * 
    */
    public void setId(Long id){
    this.id = id;
    }

    /**
    * 
    */
    public void setContent(String content){
    this.content = content;
    }

    /**
    * 
    */
    public void setCreatedTime(Date createdTime){
    this.createdTime = createdTime;
    }


    /**
    * 
    */
    public Long getId(){
    return this.id;
    }

    /**
    * 
    */
    public String getContent(){
    return this.content;
    }

    /**
    * 
    */
    public Date getCreatedTime(){
    return this.createdTime;
    }

}
