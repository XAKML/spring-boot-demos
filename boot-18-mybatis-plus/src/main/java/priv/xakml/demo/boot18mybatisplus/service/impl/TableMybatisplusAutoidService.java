package priv.xakml.demo.boot18mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import priv.xakml.demo.boot18mybatisplus.mapper.TableMybatisplusAutoidMapper;
import priv.xakml.demo.boot18mybatisplus.pojo.TableMybatisplusAutoid;
import priv.xakml.demo.boot18mybatisplus.service.ITableMybatisplusAutoidService;

@Service
public class TableMybatisplusAutoidService extends ServiceImpl<TableMybatisplusAutoidMapper, TableMybatisplusAutoid>
        implements ITableMybatisplusAutoidService {

}
