package priv.xakml.demo.boot18mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.Cacheable;
import priv.xakml.demo.boot18mybatisplus.model.TestUpdateModel;
import priv.xakml.demo.boot18mybatisplus.pojo.Test;

import java.util.List;

/**
 * @author lhx
 * @date 2022-05-26 10:58
 **/
public interface ITestService extends IService<Test> {

    /**
     * 查询范围
     * @param start
     * @param end
     * @return
     */
    List<Test> getRange(long start, long end);

    /**
     * 查询范围内的某些字段
     * @param start
     * @param end
     * @return
     */
    List<Test> getRangFields(long start, long end);
    /**
     * 根据Id查询一条记录
     * @param id
     * @return
     */
    Test get(long id);

    int updateLongValueBatch(List<TestUpdateModel> list);

    boolean batchUpdateByIds(List<Long> ids);

    boolean batchUpdateByIds(List<Long> ids,int batchSize);
}
