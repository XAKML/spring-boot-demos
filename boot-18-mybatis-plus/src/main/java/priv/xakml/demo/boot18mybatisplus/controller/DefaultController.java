package priv.xakml.demo.boot18mybatisplus.controller;

import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import priv.xakml.demo.boot18mybatisplus.model.TestUpdateModel;
import priv.xakml.demo.boot18mybatisplus.pojo.Test;
import priv.xakml.demo.boot18mybatisplus.service.ITestService;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lhx
 * @date 2022-05-26 11:03
 **/
@RestController
@RequestMapping("api/default")
public class DefaultController {

    @Autowired
    private ITestService testService;

    /**
     *
     * @return
     */
    @GetMapping
    public String get(){
        QueryWrapper<Test> testQueryWrapper = new QueryWrapper<>();
        testQueryWrapper.last(" limit 1");
        Test one = this.testService.getOne(testQueryWrapper);
        if(one != null)
            return one.toString();
        else
        return "hello world";
    }

    /**
     * 查询 TEst表的一条记录，测试缓存
     */
    @GetMapping("test")
    public Test getOne(long id){
        return this.testService.get(id);
    }

    /**
     * 按照范围查询
     * @return
     */
    @GetMapping("range")
    public List<Test> getRange(){
        List<Test> range = this.testService.getRange(10, 15);
        return range;
    }

    @GetMapping("range_fields")
    public List<Test> getRangeFields(){
        List<Test> range = this.testService.getRangFields(10, 15);
        return range;
    }

    @GetMapping("sleep")
    public long getSleepTimeSpan(){
        double v = RandomUtil.randomDouble(1, 2);
        long milliseconds = (long) (1000 * v);
        return milliseconds;
    }

    /**
     *
     * @return
     */
    @GetMapping("update_batch_by_ids")
    public String updateBatchByIds(){
        boolean isOK = false;
        List<Test> range = this.testService.getRange(0,50);
        if(range != null && range.size() > 0){
            List<Long> ids = range.stream().map(Test::getId).distinct().collect(Collectors.toList());
            isOK =  this.testService.batchUpdateByIds(ids);
        }
        if(isOK)
            return "更新成功";
        else
            return "更新失败";
    }

    /**
     * 转换ip地址的表示格式 字符串--> 数字格式
     * @param ip
     * @return
     */
    @GetMapping("ip_long")
    public long getIpLongFormat(@Param("ip") String ip){
        try {
            long ipv4ToLong = NetUtil.ipv4ToLong(ip);
            return ipv4ToLong;
        }catch (java.lang.IllegalArgumentException ex){
            return -1;
        }
    }

    @GetMapping("update_batch")
    public int updateBatch(){
        List<TestUpdateModel> testUpdateModelArrayList = new ArrayList<>();
        testUpdateModelArrayList.add(new TestUpdateModel(1L,1L));
        testUpdateModelArrayList.add(new TestUpdateModel(2L,2L));
        testUpdateModelArrayList.add(new TestUpdateModel(3L,3L));
        testUpdateModelArrayList.add(new TestUpdateModel(4L,4L));
        testUpdateModelArrayList.add(new TestUpdateModel(5L,5L));
        testUpdateModelArrayList.add(new TestUpdateModel(6L,6L));
        testUpdateModelArrayList.add(new TestUpdateModel(7L,7L));

        int i = this.testService.updateLongValueBatch(testUpdateModelArrayList);
        return i;
    }

    @GetMapping("ip/query")
    public String getIpLocation(@RequestParam String ip){
        List<IpInfo> ipGeolocationFromIP138 = IPUtils.getIPGeolocationFromIP138(ip);
        return ipGeolocationFromIP138.toString();
    }

}
