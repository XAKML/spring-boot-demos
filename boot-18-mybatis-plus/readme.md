@Transactional注解的　rollbackFor 可以设置回滚的操作的触发异常，  
Spring默认抛出了未检查unchecked异常（继承自 RuntimeException的异常）或者 Error才回滚事务，
其他异常不会触发回滚事务。  
如果在事务中抛出其他类型的异常，但却期望 Spring 能够回滚事务，就需要指定 rollbackFor属性。
![img ](https://img2020.cnblogs.com/blog/1401949/202107/1401949-20210707211753371-597096491.png)

2024年6月27日  
启动类添加CommandRunner接口， 实现启动时即可测试部分业务逻辑方法及类型

```java
public enum IdType {
    /***
     * 使用数据库自增 ID 作为主键。
     */
    AUTO(0),

    /***
     * 特定生成策略，如果全局配置中有 IdType 相关的配置，则会跟随全局配置。
     */
    NONE(1),

    /***
     * 在插入数据前，由用户自行设置主键值。
     */
    INPUT(2),

    /***
     * 自动分配 ID，适用于 Long、Integer、String 类型的主键。默认使用雪花算法通过 IdentifierGenerator 的 nextId 实现
     * 从mybatis-plus 3.3.0 版本开始
     */
    ASSIGN_ID(3),

    /***
     * 自动分配 UUID，适用于 String 类型的主键。默认实现为 dentifierGenerator 的 nextUUID 方法。
     * @since 3.3.0 
     */
    ASSIGN_UUID(4);
}
```


**使用mybatis-plus保存数据成功后，实体类能自动获取主键（自增列）的值。**  

[markdown 语法基础教程](https://markdown.com.cn/basic-syntax/images.html)