package priv.xakml.demo.nacos;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author lhx
 * @date 2021-08-09 10:47
 **/
@RestController
@RequestMapping("/a")
public class HelloNacosController {


    @GetMapping("hello")
    public String Hello(){
        return new Date().toString();
    }
}
