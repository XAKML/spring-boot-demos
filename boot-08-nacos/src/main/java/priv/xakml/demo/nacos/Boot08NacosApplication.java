package priv.xakml.demo.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@EnableDiscoveryClient
@SpringBootApplication
public class Boot08NacosApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot08NacosApplication.class, args);
    }

}
